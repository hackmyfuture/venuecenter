class CreateSubscription
  def self.call(plan, email_address, token)
    admin, raw_token = CreateAdmin.call(email_address)

    subscription = Subscription.new(
      plan: plan,
      user: user
    )

    begin
       stripe_sub = nil
       if admin.stripe_customer_id.blank?
         customer = Stripe::Customer.create(
           source: token,
           email: admin.email,
           plan: plan.stripe_id
         )
         admin.stripe_customer_id = customer.id
         admin.save!
         stripe_sub = customer.subscriptions.first
       else
         customer = Stripe::Customer.retrieve(admin.stripe_customer_id)
         stripe_sub = customer.subscriptions.create(
           plan: plan.stripe_id
         )
       end

       subscription.stripe_id = stripe.sub.id

       subscription.save!
    rescue Stripe::StripeError => e
      subscription.errors[:base] << e.message
     end

    subscription
   end
end
