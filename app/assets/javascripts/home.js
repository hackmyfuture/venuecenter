$(document).ready(function(){
  var i = 0;
  var images = ['/assets/hm-2.jpg','/assets/hm-3.jpg','/assets/hm-4.jpg','/assets/hm-5.jpg'];
  var image = $('.home-header');

  //Initial Background image setup
  //image.css('background-image', 'url(image1.png)');

  //Change image at regular intervals
  setInterval(function(){
    image.fadeOut(1000, function () {
    image.css('background-image', 'url(' + images [i++] +')');
    image.fadeIn(1000);
    });
    if(i == images.length)
      i = 0;
    }, 5000);
});
