class VenueController < ApplicationController
  def index
    @venue = Venue.friendly.find(params[:slug])

    # Handle venue not found
    if @venue.nil?
      flash[:notice] = 'The venue you tried to access was not found'
      redirect_to home_path
    else
      impressionist(@venue) if Rails.env.production?

      unless session['search_id'].blank?
        @search = VenueSearch.find(session['search_id'])
        @search_info = JSON.parse(@search.search_options)
      end
      @reviews = VenueReview.select('*').joins(:user).where('venue_reviews.venue_id = ? && users.deleted_at IS NULL', @venue.id).order(created_at: :desc)

      @review = VenueReview.new

      @rfp = Rfp.new

      @venue_images = VenueImage.where('venue_id = ?', @venue.id)
      @total_reviews = @reviews.count

      @spaces = VenueSpace.where('venue_id = ?', @venue.id)

      if @venue.billing_plan_id == 1 || @venue.billing_plan_id == 4
        @nearby_venues = Venue.near(@venue, 10).where('venue_capacity_id >= ?', @venue.venue_capacity_id).limit(6).offset(1).order('billing_plan_id desc')
      end
      # if !@venue.yelp_id.blank?
      #   #begin
      #     @yelp_data = Yelp.client.business(URI.escape(@venue.yelp_id))
      #   #rescue
      #     if @yelp_data.nil?
      #       @yr = 0
      #     elsif @yelp_data.review_count > 3
      #       @yr = 3
      #     else
      #       @yr = @yelp_data.review_count
      #     end
      #     @total_reviews = @total_reviews + @yr
      #   #end
      # end

      if user_signed_in?
        @saved = UserSavedVenue.where('user_id = ? && venue_id = ?', current_user.id, @venue.id)
      end
    end
  end

  rescue_from ActiveRecord::RecordNotFound do
    render :not_found
  end
end
