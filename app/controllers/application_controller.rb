class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  # include SessionsHelper
  before_action :configure_permitted_parameters, if: :devise_controller?

  after_action :store_location

  def blog
    redirect_to "http://venuecenter.com/blog#{request.fullpath.gsub('/blog', '')}", status: :moved_permanently
  end

  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    return unless request.get?

    # if (new_user_session_url.nil?)
    if request.path != '/users/login' &&
       request.path != '/users/sign-up' &&
       request.path != '/users/password/new' &&
       request.path != '/users/password/edit' &&
       request.path != '/users/confirmation' &&
       request.path != '/users/logout' &&
       !request.xhr? # don't store ajax calls
      session[:previous_url] = request.path
    end
    # else
    #  session[:previous_url] = new_user_session_url
    # end
  end

  def redirect_back_or_default(_default)
    session[:previous_url] || home_path
  end

  def after_sign_in_path_for(_resource_or_scope)
    session[:previous_url] || home_path
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:first_name, :last_name, :email, :password, :password_confirmation) }
  end

  def not_found
    raise ActionController::RecordNotFound, 'Not Found'
  end
end
