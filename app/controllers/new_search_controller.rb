class NewSearchController < ApplicationController
  require 'fuzzy_match'

  def index
    @conditions = {}
    @conditions[:active] = true
    @sort_conditions = {}

    @rc = if params[:rc].blank?
            20
          else
            params[:rc]
          end
    @order = if !params[:order].blank?
               params[:order]
             else
               1
             end

    # Amenities
    @conditions[:av_hookup] = true if params[:av]
    @conditions[:corkage] = true if params[:cork]
    @conditions[:handicap_accessible] = true if params[:handi]
    @conditions[:wifi] = true if params[:wifi]
    @conditions[:stage_available] = true if params[:stage]
    @conditions[:pet_friendly] = true  if params[:pets]
    @conditions[:beachfront] = true if params[:beach]
    @conditions[:city_view] = true if params[:cv]
    @conditions[:garden_view] = true if params[:gv]
    @conditions[:ocean_view] = true if params[:ov]
    @conditions[:lake_view] = true if params[:lv]
    @conditions[:overnight_available] = true if params[:overnight]

    # Food options
    @conditions[:onsite_catering] = true if params[:on_cat]
    @conditions[:offsite_catering] = true if params[:off_cat]
    @conditions[:kosher] = true if params[:kosher]
    @conditions[:vegetarian] = true if params[:veg]
    @conditions[:vegan] = true if params[:vegan]
    @conditions[:gluten_free] = true if params[:gluten]
    @conditions[:halal] = true if params[:halal]

    # Drink options
    @conditions[:wine] = true if params[:wine]
    @conditions[:beer] = true if params[:beer]
    @conditions[:full_bar] = true if params[:full_bar]
    @conditions[:outside_alcohol] = true if params[:out_alc]
    @conditions[:no_alcohol] = true if params[:no_alc]

    # Parking options
    @conditions[:free_parking] = true if params[:fp]
    @conditions[:self_parking] = true if params[:sp]
    @conditions[:valet_parking] = true if params[:vp]
    @conditions[:garage_parking] = true if params[:gp]

    # Space type options
    @conditions[:space_type] = 'Indoor' if params[:space_in]
    @conditions[:space_type] = 'Indoor/Outdoor' if params[:space_in]
    @conditions[:space_type] = 'Outdoor' if params[:space_out]
    @conditions[:space_type] = 'Indoor/Outdoor' if params[:space_out]

    # Sort results
    if @order == '3'
      # Sort by name
      @sort_conditions[:name] = { order: 'asc' }
    elsif @order == '4'
      # Sort by price low to high
      @sort_conditions[:price] = { order: 'asc' }
    elsif @order == '5'
      # Sort by price high to low
      @sort_conditions[:price] = { order: 'desc' }
    else
      # Sort by billing_plan_id
      @sort_conditions[:billing_plan_id] = { order: 'desc' }
    end

    if params[:q].present?
      # Check if anything has been sent to query
      @query_string = params[:q]
      @query_display = params[:q]

      # to extract place from query
      @cities = Venue.uniq.pluck(:city) * '|'
      @states = State.uniq.pluck(:name) * '|'
      @address = @cities + '|' + @states
      regex = Regexp.new(@address, Regexp::IGNORECASE)
      @place = begin
             @query_string.match(regex)[0]
               rescue StandardError
                 nil
           end
      @conditions[:location] = { near: Geocoder.coordinates(@place), within: '25miles' } if @place
      @query_string = begin
                    @query_string.split.delete_if { |x| @place.include?(x) }.join(' ')
                      rescue StandardError
                        nil
                  end
      # if @order == '2'
      if @place.present?
        @sort_conditions[:_geo_distance] = { location: "#{Geocoder.coordinates(@place)[0]},#{Geocoder.coordinates(@place)[1]}", order: 'asc' }
        #             else
        # #     @sort_conditions[:_geo_distance] = {location: "#{Geocoder.coordinates("")[0]},#{Geocoder.coordinates("")[1]}",order: "asc"}
        #                 @sort_conditions[:_geo_distance] = {order: 'distance'}
      end
      # end
      #     to extract any options
      @column_names = %w[onsite_catering offsite_catering outside_vendors wine beer full_bar outside_alcohol no_alcohol kosher halal vegetarian gluten_free
                         vegan free_parking self_parking valet_parking garage_parking corkage av_hookup handicap_accessible pet_friendly kid_friendly
                         lgbt_friendly wifi pool city_view lake_view ocean_view garden_view beachfront stage_available overnight_available]
      @options_names = FuzzyMatch.new(@column_names)

      @words = params[:q].split(/\W+/)
      @words.each do |query|
        @options = @options_names.find_with_score(query)
        next unless @options && @options[1] >= 0.6 # check the matching score

        @conditions[@options[0]] = 'true'
        @options[0] = @options[0].tr!('_', ' ')
        @query_string = begin
                      @query_string.split.delete_if { |x| @options[0].include?(x) }.join(' ')
                        rescue StandardError
                          nil
                    end
      end

      # to extract capacity from query
      @capacity = begin
                @query_string.gsub(/[^0-9]/, '')
                  rescue StandardError
                    nil
              end
      @query_string = begin
                    @query_string.split.delete_if { |x| @capacity.include?(x) }.join(' ')
                      rescue StandardError
                        nil
                  end
      if @capacity.present?
        @conditions[:venue_capacity_id] = 1 if @capacity.to_f > 0 && @capacity.to_f <= 75
        @conditions[:venue_capacity_id] = 2 if @capacity.to_f > 76 && @capacity.to_f <= 150
        @conditions[:venue_capacity_id] = 4 if @capacity.to_f > 251 && @capacity.to_f <= 400
        @conditions[:venue_capacity_id] = 3 if @capacity.to_f > 151 && @capacity.to_f <= 250
        @conditions[:venue_capacity_id] = 5 if @capacity.to_f > 400
      end

      # Match on venue and event type
      @query_string1 = []
      @venue_types_names = FuzzyMatch.new(VenueType.uniq.pluck(:name))
      @words = params[:q].split(/\W+/)
      @words.each do |query|
        @venue_types = @venue_types_names.find_with_score(query)
        if @venue_types && @venue_types[1] >= 0.6
          @query_string1.push @venue_types[0]
        end
      end
      @event_types_names = FuzzyMatch.new(EventType.uniq.pluck(:name))
      @words.each do |query|
        @event_types = @event_types_names.find_with_score(query)
        if @event_types && @event_types[1] >= 0.6
          @query_string1.push @event_types[0]
        end
      end
      @query_string1 = @query_string1.uniq.join(' ') if @query_string1.present?
      # END---Match on venue and event type

      @words_to_avoid = Regexp.new('in|want|venue|venues|i|space|with|and|for|a|capacity|people|guest|guests|person|the|persons', Regexp::IGNORECASE)
      @words_from_query = begin
                        @query_string.scan(@words_to_avoid)
                          rescue StandardError
                            nil
                      end
      @query_string = begin
                    @query_string.split.delete_if { |x| @words_from_query.include?(x) }.join(' ')
                      rescue StandardError
                        nil
                  end

      if @query_string1.present? # if venue or event or name is present
        @venues = Venue.search(@query_string1, where: @conditions, page: params[:page], per_page: @rc, order: @sort_conditions)
      elsif @query_string.present?
        @venues = Venue.search(@query_string, where: @conditions, operator: 'or', page: params[:page], per_page: @rc, order: @sort_conditions)
      else
        @venues = Venue.search(where: @conditions, operator: 'or', page: params[:page], per_page: @rc, order: @sort_conditions)
      end
      #     if @venues.blank?
      #         @venues = Venue.search(params[:query],where: @conditions,operator: "or", page: params[:page], per_page: @rc,order: @sort_conditions)
      #     end #end---venue or event is present

    else
      @venues = Venue.search where: @conditions, page: params[:page], per_page: @rc, order: @sort_conditions
      end # end---params[:query].present?
  end

  def get_info
    @lookup_data = []
    @user_email = if user_signed_in?
                    current_user.email
                  else
                    params[:email]
                  end
    @desc = params[:description]
    @ip = request.remote_ip
    @criteria = params[:lookup_criteria]

    NolookupResults.admin_email(@user_email, @desc, @ip, @criteria).deliver_now

    redirect_to '/lookup/confirm-submit'
  end

  def confirm_submit; end

  def events
    # Check for city lookup
    if !params[:state].blank? && !params[:city].blank?
      @loc = params[:city].titleize + ', ' + params[:state].upcase
    end
    # Check for location lookup
    @loc = params[:loc] unless params[:loc].blank?
    # Set sort order
    @order = if !params[:order].blank?
               params[:order]
             else
               1
             end

    if !params[:event_type].blank?
      @lookup_type = EventType.where('slug = ?', params[:event_type].downcase).first
      @stype = @lookup_type.name
    else
      @stype = 'Event'
    end

    if !@loc.blank?
      # Get lookup radius
      @radius = if params[:r].blank?
                  50
                else
                  params[:r]
                end
      # Sort matched venues

      if @order == '2'
        @venues = Venue.near(@loc, @radius, order: 'distance, billing_plan_id desc, name').where('active=1')
      elsif @order == '3'
        @venues = Venue.near(@loc, @radius, order: 'name, billing_plan_id desc, distance').where('active=1')
      elsif @order == '4'
        @venues = Venue.near(@loc, @radius, order: 'price asc, billing_plan_id desc, distance, name').where('active=1')
      elsif @order == '5'
        @venues = Venue.near(@loc, @radius, order: 'price desc, billing_plan_id desc, distance, name').where('active=1')
      else
        @venues = Venue.near(@loc, @radius, order: 'billing_plan_id desc, distance, name').where('active=1')
      end

      @location = Geocoder.lookup(@loc)

    else
      # List all venues
      if @order == '3'
        @venues = Venue.all.order('name, billing_plan_id desc').where('active=1')
      elsif @order == '4'
        @venues = Venue.all.order('price asc, billing_plan_id desc, name').where('active=1')
      elsif @order == '5'
        @venues = Venue.all.order('price desc, billing_plan_id desc, name').where('active=1')
      else
        @venues = Venue.all.order('billing_plan_id desc, name').where('active=1')
      end

    end
    # lookup filters
    unless params[:event_type].blank?
      @venues = @venues.joins(:event_types).where('event_types.slug = ?', @lookup_type.slug)
    end

    unless params[:venue_type].blank?
      @venues = @venues.where('venue_type_id = ?', params[:venue_type])
    end
    unless params[:cap].blank?
      @venues = @venues.where('venue_capacity_id >= ?', params[:cap])
    end
    unless params[:name].blank?
      @venues = @venues.where('name LIKE ?', '%' + params[:name] + '%')
    end
    if params[:space_in] && params[:space_out]
      @venues = @venues.where('space_type = ? || space_type = ? || space_type = ', 'Indoor/Outdoor', 'Indoor', 'Outdoor')
    elsif params[:space_in]
      @venues = @venues.where('space_type != ?', 'Outdoor')
    elsif params[:space_out]
      @venues = @venues.where('space_type != ?', 'Indoor')
    end
    # Price lookup
    @venues = @venues.where('price <= 1') if params[:p1]
    @venues = @venues.where('price <= 2') if params[:p2]
    @venues = @venues.where('price <= 3') if params[:p3]
    @venues = @venues.where('price <= 4') if params[:p4]
    # Amenities
    @venues = @venues.where(av_hookup: true) if params[:av]
    @venues = @venues.where(corkage: true) if params[:cork]
    @venues = @venues.where(handicap_accessible: true) if params[:handi]
    @venues = @venues.where(wifi: true) if params[:wifi]
    @venues = @venues.where(stage_available: true) if params[:stage]
    @venues = @venues.where(pet_friendly: true) if params[:pets]
    @venues = @venues.where(beachfront: true) if params[:beach]
    @venues = @venues.where(city_view: true) if params[:cv]
    @venues = @venues.where(garden_view: true) if params[:gv]
    @venues = @venues.where(ocean_view: true) if params[:ov]
    @venues = @venues.where(lake_view: true) if params[:lv]
    @venues = @venues.where(overnight_available: true) if params[:overnight]

    # Food options
    @venues = @venues.where(onsite_catering: true) if params[:on_cat]
    @venues = @venues.where(offsite_catering: true) if params[:off_cat]
    @venues = @venues.where(kosher: true) if params[:kosher]
    @venues = @venues.where(vegetarian: true) if params[:veg]
    @venues = @venues.where(vegan: true) if params[:vegan]
    @venues = @venues.where(gluten_free: true) if params[:gluten]
    @venues = @venues.where(halal: true) if params[:halal]
    # Drink options
    @venues = @venues.where('wine = 1 OR full_bar = 1') if params[:wine]
    @venues = @venues.where('beer = 1 OR full_bar = 1') if params[:beer]
    if params[:full_bar]
      @venues = @venues.where('full_bar = 1 OR wine = 1 OR beer = 1')
    end
    @venues = @venues.where(outside_alcohol: true) if params[:out_alc]
    @venues = @venues.where(no_alcohol: true) if params[:no_alc]

    # Parking options
    @venues = @venues.where(free_parking: true) if params[:fp]
    @venues = @venues.where(self_parking: true) if params[:sp]
    @venues = @venues.where(valet_parking: true) if params[:vp]
    @venues = @venues.where(garage_parking: true) if params[:gp]

    @rc = if !params[:rc].blank?
            params[:rc]
          else
            20
           end
    @venues = @venues.paginate(page: params[:page], per_page: @rc)
    # lookup @locs for Google Map
    @locs = @venues

    # Log lookup to database
    @lookup = Venuelookup.new
    @lookup.location = @loc
    @lookup.user_id = current_user.id if user_signed_in?
    @lookup.lookup_options = params
    @lookup.ip_address = request.remote_ip
    @lookup.num_results = @venues.count
    @lookup.save

    # Notify admin if less than 10 venues are returned by lookup
    if @venues.count < 10 && params[:name].blank?
      VenuelookupMailer.admin_email(@lookup).deliver_now
    end
  end
end
