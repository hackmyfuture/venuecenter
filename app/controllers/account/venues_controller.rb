class Account::VenuesController < ApplicationController
  before_action :authenticate_user!

  def index
    @venues = UserSavedVenue.where('user_id = ?', current_user.id).paginate(page: params[:page], per_page: 20)
  end

  def new
    @sv = UserSavedVenue.new
    @sv.user_id = current_user.id
    @sv.venue_id = params[:venue_id]
    if @sv.save
      redirect_to :back, notice: 'Venue was saved.'
    else
      render :back
    end
  end

  def destroy
    @venue = UserSavedVenue.find(params[:id])
    @venue.destroy
    respond_to do |format|
      format.html { redirect_to '/account/venues', notice: 'Saved venue was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

  # def user_saved_venue_params
  # params.require(:user_saved_venue).permit(:user_id, :venue_id, :user_event_id)
  # end
end
