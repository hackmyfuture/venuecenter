class Account::ListsController < ApplicationController
  def index
    @lists = UserList.where('user_id = ?', current_user.id)
  end

  def new; end

  def show
    @list = UserList.find(params[:id])
    # @list_venues = UserListsVenues.where('user_list_id = ?', params[:id])
  end
end
