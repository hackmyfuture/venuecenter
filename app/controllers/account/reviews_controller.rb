class Account::ReviewsController < ApplicationController
  before_action :authenticate_user!

  def index
    @reviews = VenueReview.where('user_id = ?', current_user.id)
  end

  def new
    @review = VenueReview.new
    @venue = Venue.friendly.find(params[:v])
  end

  def create
    @review = VenueReview.new(venue_review_params)
    @review.user_id = current_user.id

    respond_to do |format|
      if @review.save
        # Send emails
        VenueReviewMailer.admin_email(@review).deliver_now
        VenueReviewMailer.manager_email(@review).deliver_now

        format.html { redirect_to account_reviews_path, notice: 'Your review was successfully submitted.' }
        format.json { render :show, status: :created, location: @review }
      else
        format.html { render :new }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    # @review = VenueReview.where('id = ? & user_id = ?', params[:id], current_user.id)
    @review = VenueReview.find(params[:id])
  end

  def edit; end

  def update
    respond_to do |format|
      if @review.update(venue_review_params)
        format.html { redirect_to account_review_url(@review), notice: 'Your review was successfully updated.' }
        format.json { render :show, status: :ok, location: @quote }
      else
        format.html { render :edit }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @review.destroy
    respond_to do |format|
      format.html { redirect_to account_reviews_url, notice: 'Review was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_review
    @review = VenueReview.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def venue_review_params
    params.require(:venue_review).permit(:venue_id, :user_id, :title, :text, :service_quality, :venue_quality, :staff_quality,
                                         :corporate_event, :party, :wedding, :retreat, :status)
  end
end
