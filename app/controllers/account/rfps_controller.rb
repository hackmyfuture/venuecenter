class Account::RfpsController < ApplicationController
  before_action :authenticate_user!, except: %i[new create confirmation]
  before_action :set_rfp, only: %i[show edit update destroy confirmaton]

  def index
    @rfps = Rfp.where('user_id = ?', current_user.id)
  end

  def new
    @venue = Venue.friendly.find(params[:venue_id])

    @rfp = if !session['current_rfp'].blank?
             Rfp.find(session['current_rfp'])
           else
             Rfp.new
           end
  end

  def create
    # Check if user signed in
    if user_signed_in?
      @uid = current_user.id
    else
      if User.exists?(email: rfp_params[:email])
        @u = User.where('email = ?', rfp_params['email']).first
        @uid  = @u.id
      else
        # Email doesnt exist, create new Account
        @user = User.new(first_name: rfp_params['first_name'], last_name: rfp_params['last_name'], email: rfp_params['email'], display_name: rfp_params['first_name'] + ' ' + rfp_params['last_name'], password: 'password', password_confirmation: 'password')
        @user.confirm
        @user.save
        sign_in @user

        # Set user id
        @uid = @user.id
      end
    end

    @rfp = Rfp.new(rfp_params)
    @rfp.id = SecureRandom.hex(5).upcase

    @rfp.user_id = @uid

    @rfp.first_name = rfp_params['first_name']
    @rfp.last_name = rfp_params['last_name']
    @rfp.email = rfp_params['email']
    @rfp.ip_address = request.remote_ip
    @rfp.session_id = session.id
    @rfp.rfp_status_id = 1
    @rfp.external_rfp_status_id = 1

    # Check for existing user event
    if session['user_event_id'].blank?
      # No event exists, create a new one

      event_name = rfp_params['first_name'] + ' ' + rfp_params['last_name'] #+ ' ' +  rfp_params['event_type.name']
      @event = UserEvent.new(user_id: @uid, event_type_id: rfp_params['event_type_id'], num_guests: rfp_params['num_guests'], description:  rfp_params['message'], date_flexible:  rfp_params['date_flexible'], budget:  rfp_params['max_budget'],
                             overnight_accomodations: rfp_params['overnight_needed'], desired_date:  rfp_params['desired_date'], event_name:  event_name, need_flight:  rfp_params['need_flight'],
                             contact_phone: rfp_params['contact_phone'], ip_address: request.remote_ip)
      @event.save
      session['user_event_id'] = @event.id
    end
    @rfp.user_event_id = session['user_event_id']

    respond_to do |format|
      if @rfp.save
        session['current_rfp'] = @rfp.id
        # Venue.update_counters @venue.id, monthly_rfp_count: 1
        RfpMailer.admin_email(@rfp).deliver_now
        RfpMailer.manager_email(@rfp).deliver_now
        RfpMailer.user_email(@rfp).deliver_now
        format.html { redirect_to account_rfp_confirmation_path(@rfp.id) }
        format.json { render :show, status: :created, location: @rfp }
      else
        format.html do
          @venue = Venue.friendly.find(@rfp.venue_id)
          render :new
        end
        format.json { render json: @rfp.errors, status: :unprocessable_entity }
      end
    end
  end

  def show; end

  def edit; end

  def update
    respond_to do |format|
      if @rfp.update(venue_params)
        format.html { redirect_to account_rfp_url(@rfp), notice: 'Your RFP request was successfully updated.' }
        format.json { render :show, status: :ok, location: @rfp }
      else
        format.html { render :edit }
        format.json { render json: @rfp.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @rfp.destroy
    respond_to do |format|
      format.html { redirect_to account_rfps_url, notice: 'RFP was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def confirmation
    @rfp = Rfp.find(params[:id])
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_rfp
    @rfp = Rfp.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def rfp_params
    params.require(:rfp).permit(:id, :user_id, :venue_id, :event_type_id, :contact_phone, :num_guests, :desired_date,
                                :date_flexible, :max_budget, :message, :overnight_needed, :need_flight, :status, :ip_address, :session_id, :first_name, :last_name, :email, :rfp_status_id)
  end
end
