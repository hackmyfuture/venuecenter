class Account::EventsController < ApplicationController
  before_action :authenticate_user!

  def index
    @user_events = UserEvent.where('user_id = ?', current_user.id)
  end

  def new
    @event = UserEvent.new
  end

  def create
    @event = UserEvent.new(user_event_params)
    @event.user_id = current_user.id

    respond_to do |format|
      if @event.save
        format.html { redirect_to account_events_path, notice: 'Your event was created successfully.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @user_event = UserEvent.find(params[:id])
  end

  def edit; end

  def update; end

  def destroy; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = UserEvent.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_event_params
    params.require(:user_event).permit(:user_id, :event_type_id, :name, :start_date, :end_date, :num_guests, :max_budget, :notes, :status)
  end
end
