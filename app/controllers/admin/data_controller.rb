class Admin::DataController < ApplicationController
  def userevents
    @rfps = Rfp.where('user_event_id is null')

    @rfps.each do |rfp|
      if User.exists?(email: rfp.email)
        # User Exists, create event
        u = User.find_by(email: rfp.email)
        @uid = u.id

        # Check for existing user event
        if UserEvent.exists?(user_id: @uid)
          # Get user event id
          @event = UserEvent.find_by(user_id: @uid)
          # Update rfp with user_event_id
          rfp.update(user_event_id: @event.id)
        else
          # Create user event
          @event = UserEvent.new(user_id: @uid, event_type_id: rfp.event_type_id, num_guests: rfp.num_guests, description: rfp.message, date_flexible: rfp.date_flexible, budget: rfp.max_budget, overnight_accomodations: rfp.overnight_needed,
                                 desired_date: rfp.desired_date, event_name: rfp.first_name + ' ' + rfp.last_name + ' ' + rfp.event_type.name, need_flight: rfp.need_flight, contact_phone: rfp.contact_phone, ip_address: rfp.ip_address)
          @event.save
          # Update rfp with user_event_id
          rfp.update(user_event_id: @event.id)
        end

      else
        # No user, create user
        @user = User.new(first_name: rfp.first_name, last_name: rfp.last_name, email: rfp.email, display_name: rfp.first_name + ' ' + rfp.last_name, password: 'password', password_confirmation: 'password')
        @user.confirm
        @user.save
        @uid = @user.id

        # Check for existing user event
        if UserEvent.exists?(user_id: @uid)
          # Get user event id
          @event = UserEvent.find_by(user_id: @uid)
          # Update rfp with user_event_id
          rfp.update(user_event_id: @event.id)
        else
          # Create user event
          @event = UserEvent.new(user_id: @uid, event_type_id: rfp.event_type_id, num_guests: rfp.num_guests, description: rfp.message, date_flexible: rfp.date_flexible, budget: rfp.max_budget, overnight_accomodations: rfp.overnight_needed,
                                 desired_date: rfp.desired_date, event_name: rfp.first_name + ' ' + rfp.last_name + ' ' + rfp.event_type.name, need_flight: rfp.need_flight, contact_phone: rfp.contact_phone, ip_address: rfp.ip_address)
          @event.save
          # Update rfp with user_event_id
          rfp.update(user_event_id: @event.id)
        end
      end
    end
    @unprocessed = Rfp.where('user_id is null')
  end

  def yelp
    @location = 'Toronto, ON'
    @space_type = 'Event Space'
    @params = { term: @space_type, limit: 1 }

    # Get number of pages in resultset
    @places = Yelp.client.search(@location, @params)
    @num_pages = (@places.total / 15).ceil

    @data = []
    (1..@num_pages).each do |i|
      @offset = (i - 1) * 15
      @data += Yelp.client.search(@location, term: @space_type, limit: 15, offset: @offset).businesses
    end

    @mappedData = @data.map do |l|
      {
        yelp_id: l.id,
        name: l.name,
        # main_image: l.respond_to?("image_url") ? l.image_url : '',
        # yelp: l.url,
        phone: l.respond_to?('display_phone') ? l.display_phone : '',
        address: l.location.address[0],
        city: l.location.city,
        state: 52
        # lat: l.location.coordinate[0].latitude,
        # lng: l.location.coordinate[0].longitude
        # zip_code: l.respond_to?("postal_code") ? l.location.postal_code : ''
      }
    end
    @saved = 0
    @mappedData.each do |data|
      @v = Venue.create(billing_plan_id: 1, name: data[:name], address: data[:address], city: data[:city],
                        state_id: State.where('abbrev = ?', data[:state]).first, yelp_id: data[:yelp_id], active: 0, phone: data[:phone])
      # @venue.name = data[:name]
      # @venue.address = data[:address]
      # @venue.city = data[:city]
      # @venue.state_id = 18 #State.where('abbrev = ?', data[:state])
      # @venue.zip_code = data[:zip_code]
      # @venue.phone = data[:phone]
      # @venue.lat = data[:lat]
      # @venue.lng = data[:lng]
      # @venue.yelp_id = data[:yelp_id]
      # @venue.data_source = 'Yelp'
      # @venue.active = 0

      # if @venue.save
      @saved += 1
      # end
    end
  end

  def yelp2
    @updated = 0
    @venues = Venue.where('yelp_id is null AND state_id=43').limit(500)
    @venues.each do |venue|
      @params = { term: venue.name, limit: 1 }
      @location = venue.city
      @data = Yelp.client.search(@location, @params).businesses[0]
      unless @data.blank?
        venue.update(yelp_id: @data.id)
        @updated += 1
      end
    end
  end

  def categories
    @venues = Venue.where('venue_type_id IS NOT NULL')
    @venues.each do |venue|
      @data = VenueTypeVenue.create(venue_id: venue.id, venue_type_id: venue.venue_type_id)
    end
  end

  def foursquare
    @client = Foursquare2::Client.new(api_version: '20150326', client_id: 'WMXZUQMGWFKVW1ORGR5CXH3QQK3TCORK42NICXTWW2XNCHLG', client_secret: 'Z55K3WONWIBMZXKDOP321RSZDFQFLWMUXDBAJXRSSC5RX5ZT')
    @venues = Venue.where('main_image IS NULL AND state_id = 18 AND lat IS NOT NULL AND lng IS NOT NULL')
    @c = 0
    @venues.each do |data|
      @v = Venue.find(data.id)
      @coords = data.lat.to_s + ',' + data.lng.to_s
      @p = @client.search_venues(ll: @coords, query: data.name)

      next if @p.venues.empty?

      @fid = @p.venues[0].id
      @v.update(foursquare_id: @fid)
      @photos = @client.venue_photos(@fid)
      next if @photos.items.empty?

      @url = (@photos.items[0].prefix + @photos.items[0].width.to_s + 'x' + @photos.items[0].height.to_s + @photos.items[0].suffix).to_s
      @v.update(remote_main_image_url: @url)
      @c += 1
    end
  end

  def events
    @venues = Venue.where('venue_type_id = 32')

    @venues.each do |data|
      @vet = VenueEventType.create(venue_id: data.id, event_type_id: 2)
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def venue_params
    params.require(:venue).permit(:billing_plan_id, :name, :venue_type_id, :address, :address2, :city, :state_id, :zip_code, :phone, :email, :website, :facebook, :twitter, :google_plus, :yelp,
                                  :lat, :lng, :description, :price, :indoor_space, :outdoor_space, :max_capacity, :onsite_catering, :offsite_catering, :space_type, :venue_size, :outside_vendors, :pet_friendly, :wine, :beer,
                                  :full_bar, :outside_alcohol, :no_alcohol, :kosher, :halal, :vegetarian, :gluten_free, :vegan, :free_parking, :self_parking, :valet_parking, :garage_parking, :corkage, :av_hookup, :handicap_accessible,
                                  :wifi, :city_view, :lake_view, :ocean_view, :garden_view, :beachfront, :stage_available, :overnight_available, :main_image, :pinterest, :yelp_id, :data_source, :foursquare_id, :remote_main_image_url,
                                  :active)

    params.require(:rfp).permit(:user_id, :user_event_id)
  end
end
