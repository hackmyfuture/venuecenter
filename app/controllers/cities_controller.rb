class CitiesController < ApplicationController
  def index
    @austin_venues = Venue.limit(6).where('city = ? AND active = 1 AND main_image IS NOT NULL', 'Austin').order('RAND()')
    @dallas_venues = Venue.limit(6).where('city = ? AND active = 1 AND main_image IS NOT NULL', 'Dallas').order('RAND()')
    @houston_venues = Venue.limit(6).where('city = ? AND active = 1 AND main_image IS NOT NULL', 'Houston').order('RAND()')
    @san_antonio_venues = Venue.limit(6).where('city = ? AND active = 1 AND main_image IS NOT NULL', 'San Antonio').order('RAND()')
    @new_orleans_venues = Venue.limit(6).where('city = ? AND active = 1 AND main_image IS NOT NULL', 'New Orleans').order('RAND()')
  end
end
