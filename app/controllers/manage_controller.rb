class ManageController < ApplicationController
  layout 'manage'
  before_action :authenticate_admin!

  def index
    @venue = Venue.find(current_admin.active_venue_id)
    @rfps = Rfp.where('venue_id = ?', current_admin.active_venue_id)
    @reviews = VenueReview.where('venue_id = ?', current_admin.active_venue_id)
  end
end
