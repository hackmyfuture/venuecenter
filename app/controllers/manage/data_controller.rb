class Manage::DataController < ApplicationController
  def yelp
    @venues = Venue.where(lat: nil)
    # parameters = { term: params[:term], limit: 16 }
    # render json: Yelp.client.search('San Francisco', parameters)
    @count = 0

    @venues.each do |v|
      @business = Yelp.client.business(URI.encode(v.yelp_id))
      v.lat = @business.location.coordinate.latitude
      v.lng = @business.location.coordinate.longitude
      v.save
      @count += 1
    end
  end
end
