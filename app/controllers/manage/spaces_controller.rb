class Manage::SpacesController < ApplicationController
  layout 'manage'
  before_action :authenticate_admin!

  def index
    @venue = Venue.find(current_admin.active_venue_id)
    @spaces = VenueSpace.where('venue_id = ?', current_admin.active_venue_id)
  end

  def edit
    @venue = Venue.find(@venue.active_venue_id)
    @space = VenueSpace.where('id = ? AND venue_id = ?', params[:id], @venue.active_venue_id)
  end
end
