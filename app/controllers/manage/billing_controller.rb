class Manage::BillingController < ApplicationController
  layout 'manage'

  before_action :authenticate_admin!

  def index
    @bplan = BillingPlan.find(current_admin.venue.billing_plan_id)
  end
end
