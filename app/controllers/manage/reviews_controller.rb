class Manage::ReviewsController < ApplicationController
  layout 'manage'
  before_action :authenticate_admin!

  def index
    @venue = Venue.find(current_admin.active_venue_id)
    @reviews = VenueReview.where('venue_id = ?', @venue).paginate(page: params[:page], per_page: 20)
  end

  def show
    @venue = Venue.find(current_admin.active_venue_id)
    @review = VenueReview.where('id = ? AND venue_id = ?', params[:id], @venue)
  end
end
