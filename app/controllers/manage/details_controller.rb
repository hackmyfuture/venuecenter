class Manage::DetailsController < ApplicationController
  layout 'manage'

  before_action :authenticate_admin!, :set_venue
  def index; end

  def general; end

  def update_general
    respond_to do |format|
      if @venue.update(venue_params)
        format.html { redirect_to '/manage/details/general', notice: 'Venue was successfully updated.' }
        format.json { render :general, status: :ok, location: @venue }
      else
        format.html { render :general }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  def categories; end

  def update_categories
    respond_to do |format|
      if @venue.update(venue_params)
        format.html { redirect_to '/manage/details/categories', notice: 'Venue categories were successfully updated.' }
        format.json { render :categories, status: :ok, location: @venue }
      else
        format.html { render :categories }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  def features
    @venue = Venue.find(current_admin.active_venue_id)
  end

  def update_features
    respond_to do |format|
      if @venue.update(venue_params)
        format.html { redirect_to '/manage/details/features', notice: 'Venue was successfully updated.' }
        format.json { render :general, status: :ok, location: @venue }
      else
        format.html { render :general }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  def photos
    @bplan = BillingPlan.find(@venue.billing_plan_id)
    @images = VenueImage.where('venue_id = ?', @venue)
    @venue_image = VenueImage.new
  end

  def update_header_photo
    respond_to do |format|
      if @venue.update(venue_params)
        format.html { redirect_to '/manage/details/photos', notice: 'Your header photo was successfully updated.' }
        format.json { render :general, status: :ok, location: @venue }
      else
        format.html { render :general }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  def new_photos
    # Add new venue photo
    @image = VenueImage.new(params[:venue_image_params])
    @image.venue_id = current_admin.venue_id
    @image.photo = params[:venue_image]['photo']
    if @image.save
      redirect_to '/manage/details/photos', notice: 'Image was successfully uploaded.'
    else
      render :photos
    end
  end

  def directions; end

  def update_directions
    respond_to do |format|
      if @venue.update(venue_params)
        format.html { redirect_to '/manage/details/directions', notice: 'Venue was successfully updated.' }
        format.json { render :general, status: :ok, location: @venue }
      else
        format.html { render :general }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_venue
    @venue = Venue.find(current_admin.active_venue_id)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def venue_params
    params.require(:venue).permit(:name, :address, :address2, :city, :state_id, :zip_code, :phone, :email, :created_at, :updated_at, :lat, :lng, :website, :description, :price,
                                  :max_capacity, :onsite_catering, :offsite_catering, :venue_size, :venue_capacity_id, :outside_vendors, :pet_friendly, :facebook, :twitter, :google_plus, :yelp_id, :status, :active, :space_type, :claimed,
                                  :claimed_at, :billing_plan_id, :directions, :wine, :beer, :full_bar, :outside_alcohol, :no_alcohol, :kosher, :halal, :vegetarian, :gluten_free, :vegan, :free_parking, :self_parking,
                                  :valet_parking, :garage_parking, :corkage, :av_hookup, :handicap_accessible, :wifi, :city_view, :lake_view, :ocean_view, :garden_view, :beachfront, :stage_available, :overnight_available,
                                  :main_image, :pinterest, :data_source, :slug, :virtual_tour, :pool, :kid_friendly, :lgbt_friendly, :meta_keywords, :meta_description, venue_type_ids: [])
  end

  def venue_image_params
    params.require(:venue_image).permit(:photo, :venue_id)
  end
end
