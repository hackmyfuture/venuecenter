class Manage::VenuesController < ApplicationController
  layout 'manage'
  def index
    @venue  = Venue.find(current_admin.active_venue_id)
    @venues = current_admin.venues.where('venues.id != ?', @venue.id)
  end
end
