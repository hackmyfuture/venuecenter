class Manage::RfpsController < ApplicationController
  layout 'manage'

  before_action :authenticate_admin!

  def index
    @venue = Venue.find(current_admin.active_venue_id)
    @rfps = Rfp.where('venue_id = ?', @venue)
  end

  def show
    @venue = Venue.find(current_admin.active_venue_id)
    @rfp = Rfp.where('id = ? AND venue_id = ?', params[:id], current_admin.active_venue_id).first
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def rfp_params
    params.require(:rfp).permit(:id, :user_id, :venue_id, :event_type_id, :contact_phone, :num_guests, :desired_date,
                                :date_flexible, :budget_range, :message, :overnight_needed, :rfp_status_id)
  end
end
