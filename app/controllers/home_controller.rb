class HomeController < ApplicationController
  def home; end

  def index
    require 'rss'
    # @venue_types = VenueType.all()
    @cities = Venue.select('city, state_id').distinct.order(:city)
    # @event_types = EventType.all().order(:name)

    # Get blog items
    begin
      @blogs = RSS::Parser.parse(open('http://blog.venuecenter.com/feed').read, false).items[0..2]
    rescue StandardError
      @blogs = nil
    end
  end

  def about; end

  def faq; end

  def contact
    @contact_form = ContactForm.new
  end

  def tos; end

  def privacy; end

  def processcontact
    # Build hash from post parameters
    @contact_form = ContactForm.new(contact_form_params)
    if @contact_form.save
      # Send mail to admin
      ContactMailer.contact_mailer(@contact_form).deliver_now
      # Go back to contact page with success message
      redirect_to :contact, notice: 'Your information was submitted. A member of our team will be in contact with you soon.'
    else
      render 'contact'
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_form_params
    params.require(:contact_form).permit(:name, :email, :phone, :city, :state, :zip_code, :message)
  end
end
