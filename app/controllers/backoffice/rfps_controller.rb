class Backoffice::RfpsController < ApplicationController
  def destroy
    @rfp = Rfp.find(params[:id])
    @rfp.destroy

    redirect_to :back
  end
end
