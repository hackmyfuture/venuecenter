class Backoffice::VenuesController < ApplicationController
  def index
    @venues = Venue.paginate(page: params[:page], per_page: 25).order(pageviews: :desc)
  end

  def show
    @venue = Venue.friendly.find(params[:id])
    @admins = Admin.where('active_venue_id = ?', @venue.id)
    @rfps = Rfp.where('venue_id=?', @venue.id)
  end

  def new; end

  def create; end

  def edit; end

  def update; end
end
