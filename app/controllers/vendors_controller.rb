class VendorsController < ApplicationController
  def index
    @vendor = Vendor.new
  end

  def create
    @vendor = Vendor.new(vendor_params)

    if @vendor.save
      redirect_to :back, notice: 'Your information was submitted. A member of our team will be in contact with you soon.'
    end
  end

  private

  def vendor_params
    params.require(:vendor).permit(:company_type, :company_name, :first_name, :last_name, :email, :phone, :city, :state_id)
  end
end
