class Biz::RegistrationsController < Devise::RegistrationsController
  layout 'biz'

  def store_location
    session[:passthru] = params[:passthru] if params[:passthru]
  end

  def redirect_back_or_default(_default)
    session[:passthru] || biz_search_path
  end

  def after_sign_up_path_for(resource)
    redirect_back_or_default(resource)
  end
end
