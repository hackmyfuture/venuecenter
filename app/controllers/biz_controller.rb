class BizController < ApplicationController
  before_action :set_venue, only: %i[claim update destroy checkout]
  before_action :authenticate_admin!, except: %i[index relay_response]
  helper :authorize_net
  # force_ssl if: :ssl_configured?, except: [:index]
  protect_from_forgery except: :relay_response
  layout 'biz'

  def index; end

  def new
    @venue = Venue.new
  end

  def search
    # Check if search form has been completed
    if !params[:name].blank? && !params[:location].blank?
      @venues = Venue.near(params[:location], 50).where('name LIKE ?', '%' + params[:name] + '%')
    end
  end

  def claim
    # Check if venue has been previously claimed
  end

  def create
    @venue = Venue.new(venue_params)
    @venue.billing_plan_id = 1
    @venue.status = 'Claim Started'
    @venue.active = 0
    respond_to do |format|
      if @venue.save
        session[:new_venue_id] = @venue.id
        format.html { redirect_to biz_select_plan_path(@venue), notice: 'Venue was successfully created.' }
        format.json { render :show, status: :created, location: @venue }
      else
        format.html { render :new }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @venue.status = 'Claimed'
      if @venue.update(venue_params)
        session[:new_venue_id] = @venue.id
        format.html do
          redirect_to '/manage', notice: 'Venue was successfully claimed.'
        end
        format.json { render :show, status: :ok, location: @venue }
      else
        format.html { render :claim }
        format.json { render json: @venue.errors, status: :unprocessable_entity }
      end
    end
  end

  def selectplan
    session[:new_venue_id] = params[:id]
    @venue = Venue.friendly.find(params[:id])
    @venue.update(status: 'Claim Started') if @venue.status == 'New'
    @admin = Admin.find(current_admin.id)
    @admin.update(active_venue_id: @venue.id)
  end

  def checkout
    if params[:plan] == 'basic'
      @price = '000'
    elsif params[:plan] == 'standard'
      @price = '4999'
    elsif params[:plan] == 'premium'
      @price = '14999'
    end

    # Set price based on selected plan
    @bprice = @price
    @bprice = '100' if params[:plan] == 'basic'

    @venue = Venue.friendly.find(session[:new_venue_id])
    @user = User.new
  end

  def process_checkout
    require 'stripe'

    token = params[:stripeToken]
    @active_venue = Venue.find(current_admin.active_venue_id)
    begin
      customer = Stripe::Customer.create(
        description: @active_venue.name,
        source: token,
        email: current_admin.email
      )

      # process payment based on plan chosen
      if params[:plan] == 'basic'
        @bplan = 1
        charge = Stripe::Charge.create(
          amount: 100,
          currency: 'usd',
          customer: customer.id,
          capture: false,
          description: 'VenueCenter Basic Membership'
        )
      else
        # Determine billing plan
        if params[:plan] == 'standard'
          @plan = 'standard-monthly'
          @bplan = 2
        elsif params[:plan] == 'premium'
          @plan = 'premium-monthly'
          @bplan = 3
        end

        # Create stripe subscription
        @stripe_sub = customer.subscriptions.create(plan: @plan)
        # @subscription = Subscription.new(venue_id: current_admin.venue.id, billing_plan_id: @bplan)
        # @subsciption.save!
      end

      # Update venue details
      @active_venue.update(stripe_customer_id: customer.id, billing_plan_id: @bplan, status: 'Claimed', claimed: true, claimed_at: Time.now)

      redirect_to :biz_receipt
    rescue Stripe::CardError => e
      @error = e
      render :checkout
    end
  end

  # GET
  # Displays a receipt.
  def receipt
    @venue = Venue.friendly.find(session[:new_venue_id])
    # @transaction = VenueTransaction.new do |t|
    #   t.id = SecureRandom.hex(8).upcase
    #   t.venue_id = @venue.id
    #   t.transaction_type = 'Monthly Subscription'
    #   t.x_response_code = params[:x_response_code]
    #   t.x_response_reason_code = params[:x_response_reason_code]
    #   t.x_response_reason_text = params[:x_response_reason_text]
    #   t.x_auth_code = params[:x_auth_code]
    #   t.x_avs_code = params[:x_avs_code]
    #   t.x_trans_id = params[:x_trans_id]
    #   t.x_description = params[:x_description]
    #   t.x_amount = params[:x_amount]
    #   t.x_method = params[:x_method]
    #   t.x_type = params[:x_type]
    #   t.x_cust_id = params[:x_cust_id]
    #   t.x_first_name = params[:x_first_name]
    #   t.x_last_name = params[:x_last_name]
    #   t.x_address = params[:x_address]
    #   t.x_city = params[:x_city]
    #   t.x_state = params[:x_state]
    #   t.x_zip = params[:x_zip]
    #   t.x_country = params[:x_country]
    #   t.x_phone = params[:x_phone]
    #   t.x_email = params[:x_email]
    #   t.x_MD5_Hash = params[:x_MD5_Hash]
    #   t.x_cvv2_resp_code = params[:x_cvv2_resp_code]
    # end

    # @venue.update(claimed: true, claimed_at: Time.now.utc, status: 'Claimed')
    @admin = Admin.find(current_admin)
    @admin.update(active_venue_id: @venue.id)
    av = AdminVenue.create(venue_id: @venue.id, admin_id: @current_admin.id)

    # if !@transaction.save
    # format.html { render :checkout }
    # format.json { render json: @transaction.errors, status: :unprocessable_entity }
    # ClaimVenueMailer.user_email(@venue).deliver_now
    # end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_venue
    @venue = Venue.friendly.find(session[:new_venue_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def venue_params
    params.require(:venue).permit(:name, :venue_type_id, :address, :address2, :city, :state_id, :zip_code, :phone, :email, :website, :lat, :lng, :slug)
  end

  # def ssl_configured?
  #   !Rails.env.development?
  # end
end
