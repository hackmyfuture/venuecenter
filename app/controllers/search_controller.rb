class SearchController < ApplicationController
  def index
    require 'date'
    # Check for city search
    if !params[:state].blank? && !params[:city].blank?
      @loc = params[:city].titleize + ', ' + params[:state].upcase
    end
    # Check for location search
    @loc = params[:loc] unless params[:loc].blank?

    # Check for event type slug
    if !params[:event_type].blank?
      @search_type = EventType.where('slug = ?', params[:event_type].downcase).first
      @stype = @search_type.name
    else
      @stype = 'Event'
    end

    # Set sort order
    @order = if !params[:order].blank?
               params[:order]
             else
               1
             end

    if !@loc.blank?
      # Get search radius
      @radius = if params[:r].blank?
                  25
                else
                  params[:r]
                end
      # Sort matched venues

      if @order == '2'
        @venues = Venue.near(@loc, @radius, order: 'distance, billing_plan_id desc, name').where('active=1')
      elsif @order == '3'
        @venues = Venue.near(@loc, @radius, order: 'name, billing_plan_id desc, distance').where('active=1')
      elsif @order == '4'
        @venues = Venue.near(@loc, @radius, order: 'price asc, billing_plan_id desc, distance, name').where('active=1')
      elsif @order == '5'
        @venues = Venue.near(@loc, @radius, order: 'price desc, billing_plan_id desc, distance, name').where('active=1')
      else
        @venues = Venue.near(@loc, @radius, order: 'billing_plan_id desc, distance, name').where('active=1')
      end

      @location = Geocoder.search(@loc)

    else
      # List all venues
      if @order == '3'
        @venues = Venue.all.order('name, billing_plan_id desc').where('active=1')
      elsif @order == '4'
        @venues = Venue.all.order('price asc, billing_plan_id desc, name').where('active=1')
      elsif @order == '5'
        @venues = Venue.all.order('price desc, billing_plan_id desc, name').where('active=1')
      else
        @venues = Venue.all.order('billing_plan_id desc, name').where('active=1')
      end

    end
    # Search filters
    # if !params[:event_type].blank?
    #   @venues = @venues.joins(:event_types).where("event_types.slug = ?", @search_type.slug)
    # end

    unless params[:venue_type].blank?
      @venues = @venues.joins(:venue_types).where(venue_types_venues: { venue_type_id: params[:venue_type] })
    end

    # Find venues that can accomodate number of guests requested
    unless params[:g].blank?
      # Convert to integer
      @guests = params[:g].to_i

      if @guests < 76
        @venues = @venues.where('venues.venue_capacity_id >= 1')
      elsif @guests >= 76 && @guests <= 150
        @venues = @venues.where('venues.venue_capacity_id >= 2')
      elsif @guests >= 151 && @guests <= 250
        @venues = @venues.where('venues.venue_capacity_id >= 3')
      elsif @guests >= 251 && @guests <= 400
        @venues = @venues.where('venues.venue_capacity_id >= 4')
      elsif @guests > 400
        @venues = @venues.where('venues.venue_capacity_id >= 5')
      end
    end

    # Find venues based on old capacity id
    unless params[:cap].blank?
      @venues = @venues.where('venues.venue_capacity_id >= ?', params[:cap])
    end

    # Find venue based on name
    unless params[:name].blank?
      @venues = @venues.where('venues.name LIKE ?', '%' + params[:name] + '%')
    end

    # Find venue by space type
    if params[:space_in] && params[:space_out]
      @venues = @venues.where('space_type = ?', 'Indoor/Outdoor')
    elsif params[:space_in]
      @venues = @venues.where('space_type = ? OR space_type = ?', 'Indoor', 'Indoor/Outdoor')
    elsif params[:space_out]
      @venues = @venues.where('space_type = ? OR space_type =?', 'Outdoor', 'Indoor/Outdoor')
    end

    # Price search
    @venues = @venues.where('price <= 1') if params[:p1]
    @venues = @venues.where('price <= 2') if params[:p2]
    @venues = @venues.where('price <= 3') if params[:p3]
    @venues = @venues.where('price <= 4') if params[:p4]
    # Amenities
    @venues = @venues.where(av_hookup: true) if params[:av]
    @venues = @venues.where(corkage: true) if params[:cork]
    @venues = @venues.where(handicap_accessible: true) if params[:handi]
    @venues = @venues.where(wifi: true) if params[:wifi]
    @venues = @venues.where(stage_available: true) if params[:stage]
    @venues = @venues.where(pet_friendly: true) if params[:pets]
    @venues = @venues.where(beachfront: true) if params[:beach]
    @venues = @venues.where(city_view: true) if params[:cv]
    @venues = @venues.where(garden_view: true) if params[:gv]
    @venues = @venues.where(ocean_view: true) if params[:ov]
    @venues = @venues.where(lake_view: true) if params[:lv]
    @venues = @venues.where(kid_friendly: true) if params[:kf]
    @venues = @venues.where(pool: true) if params[:pool]
    @venues = @venues.where(overnight_available: true) if params[:overnight]

    # Food options
    @venues = @venues.where(onsite_catering: true) if params[:on_cat]
    @venues = @venues.where(offsite_catering: true) if params[:off_cat]
    @venues = @venues.where(kosher: true) if params[:kosher]
    @venues = @venues.where(vegetarian: true) if params[:veg]
    @venues = @venues.where(vegan: true) if params[:vegan]
    @venues = @venues.where(gluten_free: true) if params[:gluten]
    @venues = @venues.where(halal: true) if params[:halal]
    # Drink options
    @venues = @venues.where('wine = 1 OR full_bar = 1') if params[:wine]
    @venues = @venues.where('beer = 1 OR full_bar = 1') if params[:beer]
    if params[:full_bar]
      @venues = @venues.where('full_bar = 1 OR wine = 1 OR beer = 1')
    end
    @venues = @venues.where(outside_alcohol: true) if params[:out_alc]
    @venues = @venues.where(no_alcohol: true) if params[:no_alc]

    # Parking options
    @venues = @venues.where(free_parking: true) if params[:fp]
    @venues = @venues.where(self_parking: true) if params[:sp]
    @venues = @venues.where(valet_parking: true) if params[:vp]
    @venues = @venues.where(garage_parking: true) if params[:gp]

    @rc = if !params[:rc].blank?
            params[:rc]
          else
            20
          end
    @venues = @venues.paginate(page: params[:page], per_page: @rc)
    # Search @locs for Google Map
    @locs = @venues

    # Log search to database
    @search = VenueSearch.new
    @search.location = @loc
    @search.user_id = current_user.id if user_signed_in?
    @search.search_options = params.to_json
    @search.ip_address = request.remote_ip
    @search.num_results = @venues.count
    @search.user_agent = request.env['HTTP_USER_AGENT']
    @search.session_id = session.id
    @search.num_guests = params[:g]
    @search.event_type = params[:event_type] unless params[:event_type].nil?
    unless params[:ed].nil?
      @search.start_date = Date.strptime(params[:ed], '%m/%d/%Y')
    end
    @search.save
    session['search_id'] = @search.id

    # Notify admin if less than 10 venues are returned by search
    if @venues.count == 0 && params[:name].blank?
      VenueSearchMailer.admin_email(@search).deliver_now
    end
  end

  def get_info
    @search_data = []
    if user_signed_in?
      @user_email = current_user.email
      @first_name = current_user.first_name
      @last_name = current_user.last_name
    else
      @user_email = params[:email]
      @first_name = params[:first_name]
      @last_name = params[:last_name]
    end
    @budget_range = params[:budget_range]
    @num_guests = params[:num_guests]
    @desired_date = params[:desired_date]
    @desc = params[:description]
    @ip = request.remote_ip
    @criteria = params[:search_criteria]

    NoSearchResults.admin_email(@user_email, @first_name, @last_name, @budget_range, @num_guests, @desired_date, @desc, @ip, @criteria).deliver_now

    redirect_to '/search/confirm-submit'
  end

  def confirm_submit; end
end
