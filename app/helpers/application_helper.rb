module ApplicationHelper
  def formatted_price(amount)
    format('$%0.2f', amount / 100.0)
  end
end
