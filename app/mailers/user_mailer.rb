class UserMailer < ApplicationMailer
  def admin_email(user)
    @user = user
    mail(to: 'admin@venuecenter.com, sengbe@gmail.com', subject: 'New User Sign Up')
  end
end
