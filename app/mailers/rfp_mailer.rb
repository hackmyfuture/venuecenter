class RfpMailer < ApplicationMailer
  def admin_email(rfp)
    @rfp = rfp
    @url = 'http://venuecenter.com/admin/rfps/'
    mail(to: 'admin@venuecenter.com', subject: 'New RFP Received')
  end

  def manager_email(rfp)
    @rfp = rfp
    @url = 'http://venuecenter.com/manage/rfps'
    @to = if rfp.venue.email.blank?
            'admin@venuecenter.com'
          else
            rfp.venue.email
          end
    mail(to: @to, bcc: '568847@bcc.hubspot.com', subject: 'A VenueCenter user has submitted a RFP for your venue')
  end

  def user_email(rfp)
    @rfp = rfp
    mail(to: @rfp.email, subject: 'Your VenueCenter RFP has been submitted')
  end
end
