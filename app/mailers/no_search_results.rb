class NoSearchResults < ApplicationMailer
  def admin_email(email, first_name, last_name, budget_range, num_guests, desired_date, description, ip, criteria)
    @email = email
    @first_name = first_name
    @last_name = last_name
    @budget_range = budget_range
    @num_guests = num_guests
    @desired_date = desired_date
    @description = description
    @ip = ip
    @criteria = criteria
    mail(to: 'hello@venuecenter.com', subject: 'No Search Results Found')
  end
end
