class VenueReviewMailer < ApplicationMailer
  def admin_email(review)
    @review = review
    @url = 'http://venuecenter.com/admin/reviews/'
    mail(to: 'admin@venuecenter.com, shawn@venuecenter.com', subject: 'New Venue Review Received')
  end

  def manager_email(review)
    @review = review
    @url = 'http://venuecenter.com/manage/reviews/'
    @to = if @review.venue.email.blank?
            'admin@venuecenter.com'
          else
            @review.venue.email + ', admin@venuecenter.com'
          end
    mail(to: @to, bcc: 'shawn@venuecenter.com', subject: 'New Venue Review Received')
  end
end
