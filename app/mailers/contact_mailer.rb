class ContactMailer < ApplicationMailer
  def contact_mailer(details)
    @details = details
    mail(to: 'hello@venuecenter.com', subject: 'New Contact Form Submission')
  end
end
