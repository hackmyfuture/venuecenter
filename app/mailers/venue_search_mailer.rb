class VenueSearchMailer < ApplicationMailer
  def admin_email(search)
    @search = search
    mail(to: 'admin@venuecenter.com', subject: 'Alert: Low Number of Matching Venues Found For Search')
  end
end
