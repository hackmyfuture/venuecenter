class ApplicationMailer < ActionMailer::Base
  default from: 'VenueCenter <admin@venuecenter.com>'
  layout 'mailer'
end
