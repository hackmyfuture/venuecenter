ActiveAdmin.register Rfp, as: 'RFP' do
  config.sort_order = 'updated_at_desc'

  filter :venue, as: :select, collection: proc { Venue.order('name') }
  filter :event_type
  filter :max_budget, label: 'Budget Range', as: :select
  filter :email
  filter :first_name
  filter :last_name
  filter :created_at
  filter :updated_at
  filter :rfp_status
  filter :external_rfp_status

  controller do
    def scoped_collection
      super.includes :venue, :user, :event_type, :external_rfp_status, :user_event
      Rfp.includes(:rfp_status)
    end
  end

  index do
    selectable_column
    column :id
    column :user
    column :venue, sortable: 'venues.name'
    column :event_type, sortable: 'event_types.name'
    column :user_event, sortable: 'user_event.event_name'
    column :num_guests
    column :desired_date
    column :date_flexible
    column :max_budget
    column :overnight_needed
    column :need_flight
    column :rfp_status, sortable: 'rfp_statuses.description'
    column :external_rfp_status, sortable: 'external_rfp_status.name'
    column :created_at
    column :updated_at
    actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  permit_params :user_id, :venue_id, :event_type_id, :contact_phone, :num_guests, :desired_date, :date_flexible, :max_budget, :message, :overnight_needed, :status, :created_at,
                :updated_at, :first_nam, :last_name, :email, :rfp_status_id, :external_rfp_status_id, :user_event_id
end
