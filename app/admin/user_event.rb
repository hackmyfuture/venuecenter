ActiveAdmin.register UserEvent, as: 'Event' do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    selectable_column
    column :id
    column :event_name
    column :company_name
    column :desired_date
    column :user
    column :status
    column :budget
    column :num_guests
    column :event_type
    column 'Catering', :catering_type
    column 'Alcohol', :alcohol_type
    column 'Hotel', :overnight_accomodations
    column 'Flight', :need_flight
    actions
  end

  form do |f|
    f.semantic_errors
    f.inputs 'Basic Details' do
      f.input :event_name
      f.input :user
      f.input :company_name
      f.input :contact_phone
      f.input :event_type
      f.input :desired_date
      f.input :date_flexible
      f.input :budget
      f.input :num_guests
      f.input :description
      f.input :catering_type
      f.input :alcohol_type
      f.input :overnight_accomodations
      f.input :need_flight
      f.input :status
      f.input :ip_address
    end
    f.actions
  end

  permit_params :id, :user_id, :event_type_id, :num_guests, :notes, :status, :updated_at, :description, :company_name, :date_flexible, :budget, :overnight_accomodations,
                :first_name, :last_name, :catering_type, :alcohol_type, :desired_date, :email, :event_name, :need_flight, :contact_phone, :ip_address
end
