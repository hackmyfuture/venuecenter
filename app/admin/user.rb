ActiveAdmin.register User do
  index do
    selectable_column
    column :id
    column :first_name
    column :last_name
    column :email
    column :created_at
    column :last_sign_in_at
    column :confirmed_at
    column :zip_code
    column :display_name
    column :user_type
    actions
  end

  permit_params :first_name, :last_name, :email, :zip_code, :display_name, :user_type
end
