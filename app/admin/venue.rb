ActiveAdmin.register Venue do
  controller do
    def scoped_collection
      super.includes :billing_plan
    end

    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end
  filter :commissionable
  filter :rfps_count
  filter :active
  filter :billing_plan
  filter :name
  filter :slug
  filter :city
  filter :state
  filter :email
  filter :status
  filter :claimed
  filter :description
  filter :venue_size
  filter :num_spaces
  filter :largest_space_size
  filter :largest_space_capacity
  filter :guest_rooms
  filter :onsite_catering
  filter :offsite_catering
  filter :outside_vendors
  filter :wine
  filter :beer
  filter :full_bar
  filter :outside_alcohol
  filter :kosher
  filter :halal
  filter :gluten_free
  filter :vegan
  filter :vegetarian
  filter :free_parking
  filter :self_parking
  filter :valet_parking
  filter :garage_parking
  filter :corkage
  filter :av_hookup
  filter :handicap_accessible
  filter :pet_friendly
  filter :kid_friendly
  filter :lgbt_friendly
  filter :wifi
  filter :pool
  filter :city_view
  filter :lake_view
  filter :garden_view
  filter :beachfront
  filter :stage_available
  filter :overnight_available
  filter :pageviews

  menu label: 'Venues'
  index do
    selectable_column
    column :id
    column :commissionable
    column :billing_plan
    column :slug
    column :name
    column :email
    column :address
    column :city
    column :state
    column :status

    column 'Categories' do |venue|
      venue.venue_types.map(&:name).sort.join(', ')
    end
    column :created_at
    column :active
    column :pageviews
    column :rfps_count
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Basic Details' do
      f.input :billing_plan
      f.input :commissionable
      # f.input :venue_type
      f.input :name
      f.input :slug
      f.input :address
      f.input :address2
      f.input :city
      f.input :state
      f.input :zip_code
      f.input :lat
      f.input :lng
      f.input :price
      f.input :description, as: :html_editor
      f.input :directions, as: :html_editor
      f.input :space_type, as: :select, collection: ['Indoor', 'Outdoor', 'Indoor/Outdoor']
      f.input :venue_size
      f.input :venue_capacity
      f.input :main_image, hint: (f.template.image_tag(f.object.main_image.url) if f.object.main_image?)
      f.input :virtual_tour
      f.input :data_source
      f.input :pageviews
      # f.input :live_virtual_tour_code, as: :html_editor
    end

    f.inputs 'Contact Details' do
      f.input :phone
      f.input :email, as: :email
      f.input :website, as: :url
      f.input :facebook, as: :url
      f.input :twitter, as: :url
      f.input :google_plus, as: :url
      f.input :google_id
      f.input :yelp_id
      f.input :pinterest, as: :url
    end

    f.inputs do
      f.input :status
      f.input :active
      f.input :claimed
      f.input :claimed_at
    end

    f.inputs 'Amenities' do
      f.input :onsite_catering
      f.input :offsite_catering
      f.input :outside_vendors
      f.input :wine
      f.input :beer
      f.input :full_bar
      f.input :outside_alcohol
      f.input :no_alcohol
      f.input :kosher
      f.input :halal
      f.input :vegetarian
      f.input :gluten_free
      f.input :vegan
      f.input :free_parking
      f.input :self_parking
      f.input :valet_parking
      f.input :garage_parking
      f.input :corkage
      f.input :av_hookup
      f.input :handicap_accessible
      f.input :pet_friendly
      f.input :kid_friendly
      f.input :lgbt_friendly
      f.input :wifi
      f.input :pool
      f.input :city_view
      f.input :lake_view
      f.input :ocean_view
      f.input :garden_view
      f.input :beachfront
      f.input :stage_available
      f.input :overnight_available
    end

    f.inputs 'SEO' do
      f.input :meta_description
      f.input :meta_keywords
    end

    f.inputs 'Categories' do
      f.input :venue_types, as: :select, collection: VenueType.order(:name), input_html: { multiple: true, size: 10 }
      # f.has_many :venue_types, allow_destroy: :true do |cat|
      #  cat.input :venue_type
      # end
    end

    f.inputs 'Packages' do
      f.has_many :venue_packages, new_record: 'Add Package' do |p|
        p.input :name
        p.input :description, input_html: { rows: 5 }
        p.input :price
        s.input :_destroy, as: :boolean, required: false, label: 'Remove'
      end
    end

    f.inputs 'Spaces' do
      f.input :num_spaces
      f.input :guest_rooms
      f.input :largest_space_size
      f.input :largest_space_capacity
      f.has_many :venue_spaces, new_record: 'Add Space' do |s|
        s.input :name
        s.input :description, input_html: { rows: 5 }
        s.input :square_feet
        s.input :length
        s.input :width
        s.input :ceiling_height
        s.input :classroom_size
        s.input :theater_size
        s.input :banquet_size
        s.input :reception_size
        s.input :conference_size
        s.input :u_shape_size
        s.input :h_square_size
        s.input :_destroy, as: :boolean, required: false, label: 'Remove'
      end
    end
    f.inputs 'Staff' do
      f.has_many :venue_staff, new_record: 'Add Staff' do |s|
        s.input :first_name
        s.input :last_name
        s.input :title
        s.input :email
        s.input :_destroy, as: :boolean, required: false, label: 'Remove'
      end
    end

    f.actions
  end

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :venue_type_id, :commissionable, :name, :slug, :address, :address2, :city, :state_id, :zip_code, :phone, :email, :created_at, :updated_at, :lat, :lng, :website, :description, :price,
                :max_capacity, :onsite_catering, :offsite_catering, :venue_size, :venue_capacity_id, :outside_vendors, :pet_friendly, :facebook, :twitter, :google_id, :google_plus, :yelp, :status, :active, :space_type, :claimed,
                :claimed_at, :billing_plan_id, :directions, :wine, :beer, :full_bar, :outside_alcohol, :no_alcohol, :kosher, :halal, :vegetarian, :gluten_free, :vegan, :free_parking, :self_parking,
                :valet_parking, :garage_parking, :corkage, :av_hookup, :handicap_accessible, :wifi, :city_view, :lake_view, :ocean_view, :garden_view, :beachfront, :stage_available, :overnight_available,
                :main_image, :pinterest, :yelp_id, :data_source, :foursquare_id, :virtual_tour, :pageviews, :pool, :kid_friendly, :lgbt_friendly, :meta_keywords, :meta_description, :rfps_count, :num_spaces, :largest_space_size, :largest_space_capacity, :guest_rooms,
                venue_event_types_attributes: %i[id venue_id event_type_id _destroy],

                venue_packages_attributes: %i[id venue_id name description price _destroy],

                venue_spaces_attributes: %i[id venue_id name description square_feet width length ceiling_height classroom_size theater_size banquet_size reception_size conference_size
                                            u_shape_size h_square_size _destroy],

                venue_type_ids: [],

                venue_staff_attributes: %i[id venue_id first_name last_name title email _destroy]
end
