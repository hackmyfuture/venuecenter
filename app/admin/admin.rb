ActiveAdmin.register Admin, as: 'Manager' do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :first_name, :last_name, :title, :role, :email, :active_venue_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  def scoped_collection
    super.includes :venues
  end

  index do
    selectable_column
    column :first_name
    column :last_name
    column 'Venue' do |admin|
      link_to admin.venues.map(&:name).sort.join(', '), admin_venue_path(admin)
    end
    column :title
    column :role
    column :email
    column :last_sign_in_at
    column :created_at
    column :updated_at
    actions
  end

  filter :first_name
  filter :last_name
  filter :title
  filter :role
  filter :email
  filter :created_at
end
