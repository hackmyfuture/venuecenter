class ContactForm < ActiveRecord::Base
  validates_presence_of :name, :email, :phone
  attr_accessor :name, :email, :phone, :city, :state, :zip_code, :message
end
