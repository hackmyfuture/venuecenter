class Subscription < ActiveRecord::Base
  belongs_to :venue
  belongs_to :billing_plan
  belongs_to :stripe_id

  validates_uniqueness_of :stripe_id
end
