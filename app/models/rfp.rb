class Rfp < ActiveRecord::Base
  self.primary_key = :id
  belongs_to :event_type
  belongs_to :user
  belongs_to :venue, counter_cache: true
  belongs_to :user_event
  belongs_to :rfp_status
  belongs_to :external_rfp_status
  validates_presence_of :venue_id, :event_type_id, :num_guests, :max_budget, :desired_date
  validates_uniqueness_of :venue_id, scope: %i[user_id desired_date event_type_id]

  after_initialize :init

  def init
    self.external_rfp_status_id ||= 1
  end
end
