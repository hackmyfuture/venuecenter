class UserList < ActiveRecord::Base
  belongs_to :user
  has_many :user_list_venues
  has_many :venues, through: :user_list_venues
end
