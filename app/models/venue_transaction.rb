class VenueTransaction < ActiveRecord::Base
  belongs_to :venue

  before_save :populate_guid

  validates_presence_of :venue_id, :transaction_type, :stripe_id
  validates_uniqueness_of :guid

  def populate_guid
    if new_record?
      while !valid? || guid.nil?
        self.guid = SecureRandom.random_number(1_000_000_000).to_s(36)
      end
    end
  end
end
