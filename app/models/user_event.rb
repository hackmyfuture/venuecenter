class UserEvent < ActiveRecord::Base
  has_many :event_locations
  # has_many :venues, through: :user_saved_venues
  has_many :rfps
  belongs_to :user
  belongs_to :event_type
end
