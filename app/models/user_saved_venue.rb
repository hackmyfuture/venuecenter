class UserSavedVenue < ActiveRecord::Base
  belongs_to :user
  belongs_to :venue
  # belongs_to :user_event
end
