class VenueType < ActiveRecord::Base
  has_and_belongs_to_many :venues, dependent: :destroy
  validates_presence_of :name
end
