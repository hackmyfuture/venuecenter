class VenueSearch < ActiveRecord::Base
  belongs_to :user

  has_one :venue_type
end
