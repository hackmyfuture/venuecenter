class AdminVenue < ActiveRecord::Base
  belongs_to :admin
  belongs_to :venue
end
