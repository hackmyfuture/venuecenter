class VenueImage < ActiveRecord::Base
  belongs_to :venue

  # validates_presence_of :venue_id, :photo

  mount_uploader :photo, VenueImageUploader
end
