require 'elasticsearch/model'

class Venue < ActiveRecord::Base
  extend FriendlyId

  friendly_id :slug_candidates, use: :slugged
  after_create :remake_slug

  # searchkick callbacks: :async, locations: ["location"]

  # include Elasticsearch::Model
  # include Elasticsearch::Model::Callbacks
  include Impressionist::IsImpressionable

  def slug_candidates
    [
      :name,
      %i[name city],
      %i[name address city]
    ]
  end

  def remake_slug
    update_attribute(:slug, nil)
    save!
  end

  def should_generate_new_friendly_id?
    new_record? || slug.nil?
  end

  belongs_to :state
  belongs_to :billing_plan
  belongs_to :venue_capacity

  has_many :venue_spaces, dependent: :destroy
  has_many :rfps, dependent: :destroy
  has_many :venue_reviews, dependent: :destroy
  has_many :venue_images, dependent: :destroy
  has_and_belongs_to_many :admins
  has_many :venue_transactions
  has_many :venue_packages, dependent: :destroy
  has_many :user_saved_venues, dependent: :destroy
  has_and_belongs_to_many :venue_types

  has_many :venue_event_types, dependent: :destroy
  has_many :event_types, through: :venue_event_types

  mount_uploader :main_image, MainImageUploader

  has_many :user_list_venues
  has_many :user_lists, through: :user_list_venues

  has_many :venue_staff, dependent: :destroy

  accepts_nested_attributes_for :venue_packages, allow_destroy: true
  accepts_nested_attributes_for :venue_spaces, allow_destroy: true
  accepts_nested_attributes_for :venue_staff, allow_destroy: true
  accepts_nested_attributes_for :venue_types, allow_destroy: true
  accepts_nested_attributes_for :venue_event_types, allow_destroy: true

  is_impressionable counter_cache: true, column_name: :pageviews, unique: %i[impressionable_type impressionable_id session_hash]

  # validates_presence_of :name, :address, :city, :state_id, :billing_plan_id
  # validates_uniqueness_of :name, scope: [:address, :city, :state], message: 'Venue has already been taken'
  # validates_uniqueness_of :stripe_customer_id

  geocoded_by :full_address, latitude: :lat, longitude: :lng

  after_validation :geocode # , :if => :address_changed?

  def full_address
    [address, city, state].compact.join(', ')
  end

  def search_data
    attributes.merge location: [lat, lng]
  end
end

# Venue.import
