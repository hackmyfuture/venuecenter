class EventType < ActiveRecord::Base
  has_many :rfps
  has_many :venues, through: :venue_event_types
  has_many :user_events
end
