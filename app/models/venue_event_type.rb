class VenueEventType < ActiveRecord::Base
  belongs_to :venue
  belongs_to :event_type
end
