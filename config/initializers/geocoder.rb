Geocoder.configure(
  # geocoding options
  timeout: 10, # geocoding service timeout (secs)
  #  ip_lookup: :telize,
  lookup: :google, # name of geocoding service (symbol)
  use_https: true,
  # :language     => :en,         # ISO-639 language code
  # :use_https    => false,       # use HTTPS for lookup requests? (if supported)
  # :http_proxy   => nil,         # HTTP proxy server (user:pass@host:port)
  # :https_proxy  => nil,         # HTTPS proxy server (user:pass@host:port)
  api_key: 'AIzaSyBdIqKwlNyaXg5aSUSOEmqh4UYj9GPFPk8' # API key for geocoding service
  # :api_key        => 'AjiWJfHC3-N4H4Vjo9AAPj4EkViX3Fdoi1veIxSdoj29SqO58-3SgHn9ge8dgSMq'
  #:cache        => Rails.cache         # cache object (must respond to #[], #[]=, and #keys)
  # :cache_prefix => "geocoder:", # prefix (string) to use for all cache keys

  # exceptions that should not be rescued by default
  # (if you want to implement custom error handling);
  # supports SocketError and TimeoutError
  # :always_raise => [],

  # calculation options
  # :units     => :mi,       # :km for kilometers or :mi for miles
  # :distances => :linear    # :spherical or :linear
)
