# StripeEvent.event_retriever = lamba do |params|
#     return nil if StripeWebhook.exists?(stripe_id: params[:id])
#     StripeWebhook.create!(stripe_id: params[:id])
#     Stripe::Event.retrieve(params[:id])
# end

StripeEvent.configure do |events|
  events.subscribe 'charge.dispute.created' do |event|
    StripeMailer.admin_dispute_created(event.data.object).deliver
  end
end
