Rails.application.routes.draw do
  namespace :manage do
    get 'venues/index'
  end

  get '/blog' => redirect('http://blog.venuecenter.com')
  get '/tx/austin/www.WallerBallroom.com' => redirect('https://venuecenter.com/tx/austin/waller-ballroom')
  get '/tx/friendswood/friendswood-mediation-conference-center', to: redirect('/')
  get '/venues/friendswood-mediation-conference-center', to: redirect('/')

  mount StripeEvent::Engine => '/stripe-events'

  get 'concierge/index'
  get 'lists/:id' => 'account/lists#show'
  get 'new_search' => 'new_search#index', as: 'new-search'

  resources :vendors
  match '/administrator/index.php', via: %i[get post], to: redirect('/')
  get '/users/login?:data', to: redirect('/users/login')
  get '/users/sign-up?:data', to: redirect('/users/sign-up')
  get '/blog', to: redirect('http://blog.venuecenter.com')
  get '/blog/robots.txt', to: redirect('http://blog.venuecenter.com/robots.txt')
  get '/blog/:category/:name', to: redirect('http://blog.venuecenter.com')

  get '/xmlrpc.php', to: redirect('/')
  get '/index.php', to: redirect('/')
  get '/index.php/venues', to: redirect('/')
  get '/cdn-cgi/l', to: redirect('/')
  get '/plugins/system/jch_optimize/assets2/jscss.php', to: redirect('/')
  get '/users/how-it-works', to: redirect('/how-it-works')
  get '/corporate-party-venues/tx/dallas', to: redirect('/corporate-event-venues/tx/dallas')

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  match '/payments/payment', to: 'payments#payment', as: 'paymentspayment', via: [:get]
  match '/payments/relay_response', to: 'payments#relay_response', as: 'payments_relay_response', via: [:post]
  match '/payments/receipt', to: 'payments#receipt', as: 'payments_receipt', via: [:get]

  get '/cities' => 'cities#index', as: :cities_home

  get '/venue-manager' => 'biz#index', as: :biz_home
  get '/biz', to: redirect('/venue-manager')
  match 'biz/search' => 'biz#search', via: %i[get post], as: :biz_search
  get 'biz/new' => 'biz#new'
  post 'biz/create' => 'biz#create'
  get 'biz/claim/:id' => 'biz#claim', as: :biz_claim
  patch 'biz/update/:id' => 'biz#update'
  put 'biz/update/:id' => 'biz#update'
  get 'biz/select-plan/:id' => 'biz#selectplan', as: :biz_select_plan
  get 'biz/checkout' => 'biz#checkout'
  match 'biz/process-checkout' => 'biz#process_checkout', via: %i[get post]
  post 'biz/relay_response' => 'biz#relay_response', as: :biz_relay_response
  match 'biz/receipt' => 'biz#receipt', via: %i[get post], as: :biz_receipt

  namespace :backoffice do
    resources :venues
    resources :events
    resources :rfps
    resources :users
  end

  namespace :manage do
    get 'venues' => 'venues#index', as: :venues
    get 'details' => 'details#index', as: :details
    get 'details/general' => 'details#general'
    put 'details/general/update' => 'details#update_general'
    patch 'details/general/update' => 'details#update_general'
    get 'details/categories' => 'details#categories'
    put 'details/categoris/update' => 'details#update_categories'
    patch 'details/categories/update' => 'details#update_categories'
    get 'details/features' => 'details#features'
    put 'details/features/update' => 'details#update_features'
    patch 'details/features/update' => 'details#update_features'
    get 'details/photos' => 'details#photos'
    post 'details/photos/new' => 'details#new_photos'
    put 'details/header-photo/update' => 'details#update_header_photo'
    patch 'details/header-photo/update' => 'details#update_header_photo'
    get 'details/directions' => 'details#directions'
    put 'details/directions/update' => 'details#update_directions'
    patch 'details/directions/update' => 'details#update_directions'
    get 'details/spaces' => 'spaces#index'
    put 'details/spaces/edit/:id' => 'spaces#edit'
    patch 'details/spaces/update' => 'spaces#update'
    resources :rfps
    resources :reviews
    get 'billing' => 'billing#index', as: :billing
    get 'data/yelp' => 'data#yelp'
  end

  get 'manage' => 'manage#index', as: :manage_home

  devise_for :admin, path: 'biz', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register', edit: '/profile/edit' }, controllers: { sessions: 'biz/sessions',
                                                                                                                                                   registrations: 'biz/registrations', passwords: 'biz/passwords' }
  namespace :admin do
    get 'user-events' => 'data#userevents'
    get 'yelp' => 'data#yelp', as: :get_yelp_data
    get 'yelp2' => 'data#yelp2'
    get 'foursquare' => 'data#foursquare', as: :get_foursquare_data
    get 'events' => 'data#events'
    get 'venue-categories' => 'data#categories'
    resources :rfp
    resources :reviews
  end

  devise_for :users, path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'sign-up', edit: '/account/profile/edit' }, controllers: { sessions: 'sessions', registrations: 'registrations',
                                                                                                                                             omniauth_callbacks: 'omniauth_callbacks' }
  get '/users/register', to: redirect('users/sign-up')

  match '/users/:id/finish_signup' => 'users#finish_signup', via: %i[get patch], as: :finish_signup

  namespace :account do
    get 'profile' => 'profile#index', as: :profile
    resources :reviews
    resources :rfps
    get 'rfps/:venue_id/new', to: 'rfps#new', as: :new_rfp
    get 'rfps/:id/confirmation', to: 'rfps#confirmation', as: :rfp_confirmation
    resources :events
    resources :venues
    resources :lists
    post 'venues/new' => 'venues#new', as: :save_venue
    get 'profile/cancel_account' => 'profile#cancel_account', as: :cancel
  end
  get '/corporate-venues/tx/Austin', to: redirect('http://venuecenter.com/corporate-event-venues/tx/Austin')
  get '/corporate-venues/tx/Dallas', to: redirect('http://venuecenter.com/corporate-event-venues/tx/Dallas')
  get '/party-venues/tx/Dallas', to: redirect('http://venuecenter.com/adult-birthday-party-venues/tx/Dallas')
  get '/corporate-venues/tx/houston', to: redirect('http://venuecenter.com/corporate-event-venues/tx/houston')
  get '/corporate-venues/tx/Houston', to: redirect('http://venuecenter.com/corporate-event-venues/tx/Houston')
  get '/corporate-party-venues/tx/Austin', to: redirect('http://venuecenter.com/corporate-event-venues/tx/Austin')
  get '/corporate-party-venues/tx/Dallas', to: redirect('http://venuecenter.com/corporate-event-venues/tx/Dallas')
  get '/corporate-party-venues/tx/Houston', to: redirect('http://venuecenter.com/corporate-event-venues/tx/Houston')
  get '/event-venues/:state/:city' => 'search#index', as: :city_search
  get '/:event_type-venues/:state/:city' => 'search#index', as: :event_type_search
  get '/tx/houston', to: redirect('/event-venues/tx/houston')
  get '/tx/dallas', to: redirect('/event-venues/tx/dallas')
  get '/tx/austin', to: redirect('/event-venues/tx/austin')
  get '/tx/fort-worth', to: redirect('/event-venues/tx/fort-worth')
  get '/event/:state/:city', to: redirect('/event-venues/%{state}/%{city}')
  get '/cities/:state/:city' => 'search#index'
  get '/search/confirm-submit' => 'search#confirm_submit'
  post '/search/get-info' => 'search#get_info', as: :search_get_info

  match '/search/:loc' => 'search#index', via: %i[get post], as: :search_loc
  match '/search' => 'search#index', via: %i[get post], as: :search

  resources :states

  # User Accounts
  get 'account' => 'account#index', as: :account_home

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  get 'concierge' => 'concierge#index', as: :concierge_home
  get ':state/:city/:slug' => 'venue#index', as: :venue
  get 'venues/:slug' => 'venue#index', as: :old_venue
  get 'venues/not-found' => 'venue#not_found', as: :venue_not_found
  get 'about' => 'home#about'
  get 'how-it-works' => 'home#works'
  get 'faq' => 'home#faq'
  get 'contact' => 'home#contact'
  get 'tos' => 'home#tos'
  get 'privacy' => 'home#privacy'
  match 'process-contact' => 'home#processcontact', via: %i[get post]

  # You can have the root of your site routed with "root"
  root 'home#index', as: :home

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
