source 'https://rubygems.org'
ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.2.2'

gem 'mysql2', '~> 0.4.10'

gem 'bootsnap'

# Use SCSS for stylesheets
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
# gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'autoprefixer-rails'
gem 'bootstrap-sass', '~> 3.3.0'
gem 'geocoder'
gem 'nodejs-rails', '~> 0.0.1'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.9'

# gem 'puma'

gem 'json'
gem "rack-reverse-proxy", :require => "rack/reverse_proxy"

gem 'devise'
gem 'devise-encryptable'

gem 'omniauth'
gem 'omniauth-facebook'

gem 'elasticsearch'
gem 'elasticsearch-model'
gem 'elasticsearch-rails'
gem 'fuzzy_match'
gem 'ransack'
gem 'searchkick'

gem 'recaptcha', require: 'recaptcha/rails'

gem 'active_admin_editor', github: 'ejholmes/active_admin_editor'
gem 'activeadmin'
gem 'tinymce-rails'

gem 'yelp', require: 'yelp'

gem 'stripe', git: 'https://github.com/stripe/stripe-ruby'
gem 'stripe_event'

# Image processing
gem 'carrierwave'
gem 'cloudinary'

gem 'impressionist'

gem 'cancancan'
gem 'font-awesome-rails'
gem 'newrelic_rpm'
gem 'rails_12factor'
gem 'rollbar', '~> 2.16.3'

gem 'tzinfo-data'

gem 'friendly_id'
gem 'will_paginate'

gem 'rack-ssl-enforcer'

# gem 'foursquare2'

gem 'sendgrid-ruby'

# Rails caching
gem 'actionpack-action_caching'
gem 'actionpack-page_caching'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks', '~> 5.0.0.beta'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring', group: :development
gem 'web-console', '~> 2.0', group: :development

gem 'net-ssh'

# Use unicorn as the app server
# gem 'unicorn'

group :development do
  gem 'listen'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'capistrano', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano3-puma',   require: false
  gem 'rubocop', '~> 0.63.0', require: false
end

# Use debugger
# gem 'debugger', group: [:development, :test]
