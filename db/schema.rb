# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_160_925_170_028) do
  create_table 'active_admin_comments', force: :cascade do |t|
    t.string   'namespace',     limit: 255
    t.text     'body',          limit: 65_535
    t.string   'resource_id',   limit: 255,   null: false
    t.string   'resource_type', limit: 255,   null: false
    t.integer  'author_id',     limit: 4
    t.string   'author_type',   limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  add_index 'active_admin_comments', %w[author_type author_id], name: 'index_active_admin_comments_on_author_type_and_author_id', using: :btree
  add_index 'active_admin_comments', ['namespace'], name: 'index_active_admin_comments_on_namespace', using: :btree
  add_index 'active_admin_comments', %w[resource_type resource_id], name: 'index_active_admin_comments_on_resource_type_and_resource_id', using: :btree

  create_table 'admin_users', force: :cascade do |t|
    t.string   'email',                  limit: 255, default: '', null: false
    t.string   'encrypted_password',     limit: 255, default: '', null: false
    t.string   'reset_password_token',   limit: 255
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer  'sign_in_count', limit: 4, default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.string   'current_sign_in_ip',     limit: 255
    t.string   'last_sign_in_ip',        limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  add_index 'admin_users', ['email'], name: 'index_admin_users_on_email', unique: true, using: :btree
  add_index 'admin_users', ['reset_password_token'], name: 'index_admin_users_on_reset_password_token', unique: true, using: :btree

  create_table 'admins', force: :cascade do |t|
    t.string   'first_name',             limit: 255
    t.string   'last_name',              limit: 255
    t.string   'title',                  limit: 255
    t.integer  'role',                   limit: 4
    t.string   'email',                  limit: 255, default: '', null: false
    t.string   'encrypted_password',     limit: 255, default: '', null: false
    t.string   'reset_password_token',   limit: 255
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer  'sign_in_count', limit: 4, default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.string   'current_sign_in_ip',     limit: 255
    t.string   'last_sign_in_ip',        limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.integer  'active_venue_id', limit: 4
  end

  add_index 'admins', ['active_venue_id'], name: 'index_admins_on_active_venue_id', using: :btree
  add_index 'admins', ['email'], name: 'index_admins_on_email', unique: true, using: :btree
  add_index 'admins', ['reset_password_token'], name: 'index_admins_on_reset_password_token', unique: true, using: :btree

  create_table 'admins_venues', force: :cascade do |t|
    t.integer  'admin_id',   limit: 4
    t.integer  'venue_id',   limit: 4
    t.datetime 'created_at',           null: false
    t.datetime 'updated_at',           null: false
  end

  add_index 'admins_venues', ['admin_id'], name: 'index_admin_venues_on_admin_id', using: :btree
  add_index 'admins_venues', ['venue_id'], name: 'index_admin_venues_on_venue_id', using: :btree

  create_table 'authentications', force: :cascade do |t|
    t.integer  'user_id',      limit: 4
    t.string   'provider',     limit: 255
    t.string   'uid',          limit: 255
    t.string   'token',        limit: 255
    t.string   'token_secret', limit: 255
    t.datetime 'created_at',               null: false
    t.datetime 'updated_at',               null: false
  end

  add_index 'authentications', ['user_id'], name: 'index_authentications_on_user_id', using: :btree

  create_table 'billing_plans', force: :cascade do |t|
    t.string  'name', limit: 255
    t.decimal 'price', precision: 7, scale: 2
    t.integer 'images',   limit: 4
    t.integer 'priority', limit: 4
  end

  create_table 'contact_forms', force: :cascade do |t|
    t.string   'name',       limit: 255
    t.string   'email',      limit: 255
    t.string   'phone',      limit: 255
    t.string   'city',       limit: 255
    t.string   'state',      limit: 255
    t.string   'zip_code',   limit: 255
    t.text     'message',    limit: 65_535
    t.datetime 'created_at',               null: false
    t.datetime 'updated_at',               null: false
  end

  create_table 'countries', force: :cascade do |t|
    t.string 'name',   limit: 255
    t.string 'abbrev', limit: 255
  end

  create_table 'event_locations', force: :cascade do |t|
    t.integer  'user_event_id', limit: 4
    t.string   'location',      limit: 255
    t.datetime 'created_at',                null: false
    t.datetime 'updated_at',                null: false
  end

  create_table 'event_types', force: :cascade do |t|
    t.string 'name', limit: 255
    t.string 'slug', limit: 255
  end

  create_table 'external_rfp_statuses', force: :cascade do |t|
    t.string   'name', limit: 255
    t.datetime 'created_at',             null: false
    t.datetime 'updated_at',             null: false
  end

  create_table 'friendly_id_slugs', force: :cascade do |t|
    t.string   'slug',           limit: 255, null: false
    t.integer  'sluggable_id',   limit: 4,   null: false
    t.string   'sluggable_type', limit: 50
    t.string   'scope',          limit: 255
    t.datetime 'created_at'
  end

  add_index 'friendly_id_slugs', %w[slug sluggable_type scope], name: 'index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope', unique: true, using: :btree
  add_index 'friendly_id_slugs', %w[slug sluggable_type], name: 'index_friendly_id_slugs_on_slug_and_sluggable_type', using: :btree
  add_index 'friendly_id_slugs', ['sluggable_id'], name: 'index_friendly_id_slugs_on_sluggable_id', using: :btree
  add_index 'friendly_id_slugs', ['sluggable_type'], name: 'index_friendly_id_slugs_on_sluggable_type', using: :btree

  create_table 'identities', force: :cascade do |t|
    t.integer  'user_id',       limit: 4
    t.string   'provider',      limit: 255
    t.string   'uid',           limit: 255
    t.datetime 'created_at',                null: false
    t.datetime 'updated_at',                null: false
    t.string   'access_token',  limit: 255
    t.string   'refresh_token', limit: 255
    t.string   'name',          limit: 255
    t.string   'email',         limit: 255
    t.string   'nickname',      limit: 255
    t.string   'image',         limit: 255
    t.string   'phone',         limit: 255
    t.string   'urls',          limit: 255
  end

  add_index 'identities', ['user_id'], name: 'index_identities_on_user_id', using: :btree

  create_table 'impressions', force: :cascade do |t|
    t.string   'impressionable_type', limit: 255
    t.integer  'impressionable_id',   limit: 4
    t.integer  'user_id',             limit: 4
    t.string   'controller_name',     limit: 255
    t.string   'action_name',         limit: 255
    t.string   'view_name',           limit: 255
    t.string   'request_hash',        limit: 255
    t.string   'ip_address',          limit: 255
    t.string   'session_hash',        limit: 255
    t.text     'message',             limit: 65_535
    t.text     'referrer',            limit: 65_535
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.text     'params', limit: 65_535
  end

  add_index 'impressions', %w[controller_name action_name ip_address], name: 'controlleraction_ip_index', using: :btree
  add_index 'impressions', %w[controller_name action_name request_hash], name: 'controlleraction_request_index', using: :btree
  add_index 'impressions', %w[controller_name action_name session_hash], name: 'controlleraction_session_index', using: :btree
  add_index 'impressions', %w[impressionable_type impressionable_id ip_address], name: 'poly_ip_index', using: :btree
  add_index 'impressions', %w[impressionable_type impressionable_id request_hash], name: 'poly_request_index', using: :btree
  add_index 'impressions', %w[impressionable_type impressionable_id session_hash], name: 'poly_session_index', using: :btree
  add_index 'impressions', %w[impressionable_type message impressionable_id], name: 'impressionable_type_message_index', length: { 'impressionable_type' => nil, 'message' => 255, 'impressionable_id' => nil }, using: :btree
  add_index 'impressions', ['user_id'], name: 'index_impressions_on_user_id', using: :btree

  create_table 'rfp_statuses', force: :cascade do |t|
    t.string   'name', limit: 255
    t.datetime 'created_at',             null: false
    t.datetime 'updated_at',             null: false
  end

  create_table 'rfps', id: false, force: :cascade do |t|
    t.string   'id',                     limit: 255
    t.integer  'user_id',                limit: 4
    t.integer  'venue_id',               limit: 4
    t.integer  'event_type_id',          limit: 4
    t.integer  'user_event_id',          limit: 4
    t.string   'contact_phone',          limit: 255
    t.integer  'num_guests',             limit: 4
    t.date     'desired_date'
    t.boolean  'date_flexible'
    t.string   'max_budget',             limit: 255
    t.text     'message',                limit: 65_535
    t.boolean  'overnight_needed'
    t.datetime 'created_at',                                           null: false
    t.datetime 'updated_at',                                           null: false
    t.boolean  'need_flight', default: false
    t.string   'first_name',             limit: 255
    t.string   'last_name',              limit: 255
    t.string   'email',                  limit: 255
    t.string   'session_id',             limit: 255
    t.string   'ip_address',             limit: 255
    t.integer  'rfp_status_id',          limit: 4
    t.string   'event_name',             limit: 255
    t.time     'start_time'
    t.time     'end_time'
    t.integer  'external_rfp_status_id', limit: 4
    t.boolean  'locked', default: true
  end

  add_index 'rfps', ['external_rfp_status_id'], name: 'index_rfps_on_external_rfp_status_id', using: :btree
  add_index 'rfps', ['id'], name: 'index_rfps_on_id', unique: true, using: :btree
  add_index 'rfps', ['rfp_status_id'], name: 'index_rfps_on_rfp_status_id', using: :btree
  add_index 'rfps', ['user_event_id'], name: 'index_rfps_on_user_event_id', using: :btree

  create_table 'states', force: :cascade do |t|
    t.string  'name',       limit: 255
    t.integer 'country_id', limit: 4
    t.string  'abbrev',     limit: 255
  end

  add_index 'states', ['country_id'], name: 'index_states_on_country_id', using: :btree

  create_table 'stripe_webhooks', force: :cascade do |t|
    t.string   'stripe_id', limit: 255
    t.datetime 'created_at',             null: false
    t.datetime 'updated_at',             null: false
  end

  create_table 'subscriptions', force: :cascade do |t|
    t.integer  'venue_id',        limit: 4
    t.integer  'billing_plan_id', limit: 4
    t.string   'stripe_id',       limit: 255
    t.datetime 'created_at',                  null: false
    t.datetime 'updated_at',                  null: false
  end

  add_index 'subscriptions', ['billing_plan_id'], name: 'index_subscriptions_on_billing_plan_id', using: :btree
  add_index 'subscriptions', ['venue_id'], name: 'index_subscriptions_on_venue_id', using: :btree

  create_table 'user_events', force: :cascade do |t|
    t.integer  'user_id',                 limit: 4
    t.integer  'event_type_id',           limit: 4
    t.integer  'num_guests',              limit: 4
    t.text     'notes',                   limit: 65_535
    t.string   'status',                  limit: 255, default: 'Active'
    t.datetime 'created_at',                                               null: false
    t.datetime 'updated_at',                                               null: false
    t.text     'description',             limit: 65_535
    t.string   'company_name',            limit: 255
    t.boolean  'date_flexible'
    t.string   'budget', limit: 255
    t.boolean  'overnight_accomodations'
    t.string   'catering_type',           limit: 255
    t.string   'alcohol_type',            limit: 255
    t.datetime 'desired_date'
    t.string   'event_name', limit: 255
    t.boolean  'need_flight'
    t.string   'contact_phone',           limit: 255
    t.string   'ip_address',              limit: 255
  end

  create_table 'user_list_venues', id: false, force: :cascade do |t|
    t.integer 'user_list_id', limit: 4, null: false
    t.integer 'venue_id',     limit: 4, null: false
  end

  create_table 'user_lists', force: :cascade do |t|
    t.integer  'user_id',       limit: 4
    t.integer  'user_event_id', limit: 4
    t.string   'name',          limit: 255
    t.boolean  'public'
    t.datetime 'created_at',                null: false
    t.datetime 'updated_at',                null: false
  end

  add_index 'user_lists', ['user_event_id'], name: 'index_user_lists_on_user_event_id', using: :btree
  add_index 'user_lists', ['user_id'], name: 'index_user_lists_on_user_id', using: :btree

  create_table 'user_saved_venues', force: :cascade do |t|
    t.integer  'user_id',       limit: 4
    t.integer  'venue_id',      limit: 4
    t.integer  'user_event_id', limit: 4
    t.datetime 'created_at',              null: false
    t.datetime 'updated_at',              null: false
  end

  add_index 'user_saved_venues', ['user_event_id'], name: 'index_user_saved_venues_on_user_event_id', using: :btree
  add_index 'user_saved_venues', ['user_id'], name: 'index_user_saved_venues_on_user_id', using: :btree
  add_index 'user_saved_venues', ['venue_id'], name: 'index_user_saved_venues_on_venue_id', using: :btree

  create_table 'users', force: :cascade do |t|
    t.string   'first_name',             limit: 255, default: '',         null: false
    t.string   'last_name',              limit: 255, default: '',         null: false
    t.string   'email',                  limit: 255, default: '',         null: false
    t.string   'encrypted_password',     limit: 255, default: '',         null: false
    t.string   'reset_password_token',   limit: 255
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer  'sign_in_count', limit: 4, default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.string   'current_sign_in_ip',     limit: 255
    t.string   'last_sign_in_ip',        limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.string   'confirmation_token', limit: 255
    t.datetime 'confirmed_at'
    t.datetime 'confirmation_sent_at'
    t.integer  'venue_id', limit: 4
    t.datetime 'deleted_at'
    t.string   'zip_code',               limit: 15
    t.string   'display_name',           limit: 255
    t.string   'user_type',              limit: 255, default: 'Personal'
  end

  add_index 'users', ['confirmation_token'], name: 'index_users_on_confirmation_token', unique: true, using: :btree
  add_index 'users', ['email'], name: 'index_users_on_email', unique: true, using: :btree
  add_index 'users', ['reset_password_token'], name: 'index_users_on_reset_password_token', unique: true, using: :btree

  create_table 'vendors', force: :cascade do |t|
    t.string   'company_type', limit: 255
    t.string   'first_name',   limit: 255
    t.string   'last_name',    limit: 255
    t.string   'email',        limit: 255
    t.string   'phone',        limit: 255
    t.string   'city',         limit: 255
    t.integer  'state_id',     limit: 4
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.string   'company_name', limit: 255
  end

  create_table 'venue_amenities', force: :cascade do |t|
    t.string 'name', limit: 255
  end

  create_table 'venue_capacities', force: :cascade do |t|
    t.string   'name', limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  create_table 'venue_drink_options', force: :cascade do |t|
    t.string 'name', limit: 255
  end

  create_table 'venue_event_types', force: :cascade do |t|
    t.integer 'venue_id',      limit: 4
    t.integer 'event_type_id', limit: 4
  end

  add_index 'venue_event_types', ['event_type_id'], name: 'index_venue_event_types_on_event_type_id', using: :btree
  add_index 'venue_event_types', ['venue_id'], name: 'index_venue_event_types_on_venue_id', using: :btree

  create_table 'venue_food_options', force: :cascade do |t|
    t.string 'name', limit: 255
  end

  create_table 'venue_images', force: :cascade do |t|
    t.integer  'venue_id',      limit: 4
    t.string   'photo',         limit: 255
    t.string   'version',       limit: 255
    t.string   'format',        limit: 255
    t.string   'resource_type', limit: 255
    t.string   'url',           limit: 255
    t.string   'secure_url',    limit: 255
    t.boolean  'default'
    t.text     'description', limit: 65_535
    t.datetime 'created_at',                  null: false
    t.datetime 'updated_at',                  null: false
  end

  create_table 'venue_packages', force: :cascade do |t|
    t.integer  'venue_id',    limit: 4
    t.string   'name',        limit: 255
    t.text     'description', limit: 65_535
    t.decimal  'price', precision: 10
    t.datetime 'created_at',                               null: false
    t.datetime 'updated_at',                               null: false
  end

  add_index 'venue_packages', ['venue_id'], name: 'index_venue_packages_on_venue_id', using: :btree

  create_table 'venue_parking_options', force: :cascade do |t|
    t.string 'name', limit: 255
  end

  create_table 'venue_reviews', force: :cascade do |t|
    t.integer  'venue_id',        limit: 4
    t.integer  'user_id',         limit: 4
    t.string   'title',           limit: 255
    t.text     'text',            limit: 65_535
    t.integer  'service_quality', limit: 4
    t.integer  'venue_quality',   limit: 4
    t.integer  'staff_quality',   limit: 4
    t.boolean  'corporate_event'
    t.boolean  'party'
    t.boolean  'wedding'
    t.boolean  'retreat'
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  create_table 'venue_searches', force: :cascade do |t|
    t.integer  'user_id',        limit: 4
    t.string   'ip_address',     limit: 255
    t.string   'location',       limit: 255
    t.text     'search_options', limit: 65_535
    t.integer  'num_results',    limit: 4
    t.datetime 'created_at',                   null: false
    t.datetime 'updated_at',                   null: false
    t.text     'user_agent',     limit: 65_535
    t.string   'event_type',     limit: 255
    t.integer  'num_guests',     limit: 4
    t.string   'session_id',     limit: 255
  end

  create_table 'venue_spaces', force: :cascade do |t|
    t.integer  'venue_id',        limit: 4
    t.string   'name',            limit: 255
    t.integer  'square_feet',     limit: 4
    t.decimal  'width',                         precision: 10
    t.decimal  'length',                        precision: 10
    t.decimal  'ceiling_height',                precision: 10
    t.integer  'classroom_size',  limit: 4
    t.integer  'theater_size',    limit: 4
    t.integer  'banquet_size',    limit: 4
    t.integer  'reception_size',  limit: 4
    t.integer  'conference_size', limit: 4
    t.integer  'u_shape_size',    limit: 4
    t.integer  'h_square_size',   limit: 4
    t.datetime 'created_at',                                   null: false
    t.datetime 'updated_at',                                   null: false
    t.text     'description', limit: 65_535
  end

  add_index 'venue_spaces', ['venue_id'], name: 'index_venue_spaces_on_venue_id', using: :btree

  create_table 'venue_staff', force: :cascade do |t|
    t.integer  'venue_id',   limit: 4
    t.string   'first_name', limit: 255
    t.string   'last_name',  limit: 255
    t.string   'title',      limit: 255
    t.string   'email',      limit: 255
    t.datetime 'created_at',             null: false
    t.datetime 'updated_at',             null: false
  end

  add_index 'venue_staff', ['venue_id'], name: 'index_venue_staff_on_venue_id', using: :btree

  create_table 'venue_transactions', id: false, force: :cascade do |t|
    t.string   'id',                     limit: 255
    t.integer  'venue_id',               limit: 4
    t.string   'transaction_type',       limit: 255
    t.string   'x_response_code',        limit: 255
    t.string   'x_response_reason_code', limit: 255
    t.string   'x_response_reason_text', limit: 255
    t.string   'x_auth_code',            limit: 255
    t.string   'x_avs_code',             limit: 255
    t.string   'x_trans_id',             limit: 255
    t.string   'x_description',          limit: 255
    t.string   'x_amount',               limit: 255
    t.string   'x_method',               limit: 255
    t.string   'x_type',                 limit: 255
    t.string   'x_cust_id',              limit: 255
    t.string   'x_first_name',           limit: 255
    t.string   'x_last_name',            limit: 255
    t.string   'x_address',              limit: 255
    t.string   'x_city',                 limit: 255
    t.string   'x_state',                limit: 255
    t.string   'x_zip',                  limit: 255
    t.string   'x_country',              limit: 255
    t.string   'x_phone',                limit: 255
    t.string   'x_email',                limit: 255
    t.string   'x_MD5_Hash',             limit: 255
    t.string   'x_cvv2_resp_code',       limit: 255
    t.datetime 'created_at',                         null: false
    t.datetime 'updated_at',                         null: false
    t.string   'guid',                   limit: 255
    t.string   'stripe_id',              limit: 255
  end

  create_table 'venue_types', force: :cascade do |t|
    t.string 'name', limit: 255
  end

  create_table 'venue_types_venues', id: false, force: :cascade do |t|
    t.integer 'venue_type_id', limit: 4, null: false
    t.integer 'venue_id',      limit: 4, null: false
  end

  create_table 'venues', force: :cascade do |t|
    t.string   'slug',                   limit: 255
    t.string   'name',                   limit: 255
    t.string   'address',                limit: 255
    t.string   'address2',               limit: 255
    t.string   'city',                   limit: 255
    t.integer  'state_id',               limit: 4
    t.string   'zip_code',               limit: 255
    t.string   'phone',                  limit: 255
    t.string   'email',                  limit: 255
    t.float    'lat',                    limit: 24
    t.float    'lng',                    limit: 24
    t.string   'website',                limit: 255
    t.text     'description',            limit: 65_535
    t.integer  'price',                  limit: 4
    t.string   'facebook',               limit: 255
    t.string   'twitter',                limit: 255
    t.string   'google_plus',            limit: 255
    t.string   'google_id',              limit: 255
    t.string   'yelp_id',                limit: 255
    t.string   'pinterest',              limit: 255
    t.string   'status',                 limit: 255,   default: 'New'
    t.boolean  'active',                               default: true
    t.string   'space_type', limit: 255, default: 'Indoor'
    t.boolean  'claimed', default: false
    t.datetime 'claimed_at'
    t.integer  'billing_plan_id',        limit: 4, default: 1
    t.text     'directions',             limit: 65_535
    t.boolean  'onsite_catering',                      default: true
    t.boolean  'offsite_catering',                     default: true
    t.integer  'venue_size',             limit: 4
    t.integer  'venue_capacity_id',      limit: 4
    t.boolean  'outside_vendors', default: true
    t.boolean  'wine'
    t.boolean  'beer'
    t.boolean  'full_bar'
    t.boolean  'outside_alcohol'
    t.boolean  'no_alcohol'
    t.boolean  'kosher'
    t.boolean  'halal'
    t.boolean  'vegetarian'
    t.boolean  'gluten_free'
    t.boolean  'vegan'
    t.boolean  'free_parking'
    t.boolean  'self_parking'
    t.boolean  'valet_parking'
    t.boolean  'garage_parking'
    t.boolean  'corkage'
    t.boolean  'av_hookup'
    t.boolean  'handicap_accessible'
    t.boolean  'pet_friendly', default: false
    t.boolean  'kid_friendly'
    t.boolean  'lgbt_friendly'
    t.boolean  'wifi'
    t.boolean  'pool'
    t.boolean  'city_view'
    t.boolean  'lake_view'
    t.boolean  'ocean_view'
    t.boolean  'garden_view'
    t.boolean  'beachfront'
    t.boolean  'stage_available'
    t.boolean  'overnight_available'
    t.string   'main_image',             limit: 255
    t.string   'virtual_tour',           limit: 255
    t.string   'data_source',            limit: 255, default: ''
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.string   'foursquare_id',          limit: 255
    t.integer  'pageviews',              limit: 4, default: 0
    t.text     'meta_description',       limit: 65_535
    t.text     'meta_keywords',          limit: 65_535
    t.text     'live_virtual_tour_code', limit: 65_535
    t.string   'time_zone',              limit: 255, default: 'Central Time (US & Canada)'
    t.string   'stripe_customer_id',     limit: 255
    t.integer  'rfps_count',             limit: 4,     default: 0
    t.boolean  'commissionable',                       default: false
    t.integer  'num_spaces',             limit: 4
    t.integer  'largest_space_capacity', limit: 4
    t.integer  'largest_space_size',     limit: 4
    t.integer  'guest_rooms',            limit: 4
  end

  add_index 'venues', ['slug'], name: 'index_venues_on_slug', unique: true, using: :btree
  add_index 'venues', ['state_id'], name: 'state', using: :btree

  add_foreign_key 'rfps', 'user_events'
  add_foreign_key 'venue_staff', 'venues'
end
