class CreateJoinTableUserListVenues < ActiveRecord::Migration
  def change
    create_join_table :user_lists, :venues do |t|
      # t.index [:user_list_id, :venue_id]
      # t.index [:venue_id, :user_list_id]
    end
  end
end
