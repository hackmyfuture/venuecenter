class AddStatusToVenue < ActiveRecord::Migration
  def change
    add_column :venues, :status, :string, default: 'New'
    add_column :venues, :active, :boolean, default: true
  end
end
