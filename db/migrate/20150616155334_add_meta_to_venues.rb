class AddMetaToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :meta_description, :text
    add_column :venues, :meta_keywords, :text
  end
end
