class AddFieldsToVenueSearches < ActiveRecord::Migration
  def change
    add_column :venue_searches, :event_type, :string
    add_column :venue_searches, :num_guests, :integer
    add_column :venue_searches, :session_id, :string
  end
end
