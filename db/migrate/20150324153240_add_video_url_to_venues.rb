class AddVideoUrlToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :video_id, :string
  end
end
