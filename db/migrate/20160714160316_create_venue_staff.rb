class CreateVenueStaff < ActiveRecord::Migration
  def change
    create_table :venue_staff do |t|
      t.references :venue, index: true, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :title
      t.string :email

      t.timestamps null: false
    end
  end
end
