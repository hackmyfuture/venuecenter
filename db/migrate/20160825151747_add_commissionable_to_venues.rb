class AddCommissionableToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :commissionable, :boolean, default: false
  end
end
