class AddQuoteStatusToQuoteRequest < ActiveRecord::Migration
  def change
    add_reference :quote_requests, :quote_status, index: true, foreign_key: true

    remove_column :quote_requests, :status
  end
end
