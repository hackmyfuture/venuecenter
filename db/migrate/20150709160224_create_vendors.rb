class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.string :company_type
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :city
      t.references :state

      t.timestamps
    end
  end
end
