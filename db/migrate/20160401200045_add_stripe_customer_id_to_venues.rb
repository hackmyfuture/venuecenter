class AddStripeCustomerIdToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :stripe_customer_id, :string
  end
end
