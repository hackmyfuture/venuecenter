class AddMainImageToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :main_image, :string
  end
end
