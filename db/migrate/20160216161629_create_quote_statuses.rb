class CreateQuoteStatuses < ActiveRecord::Migration
  def change
    create_table :quote_statuses do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
