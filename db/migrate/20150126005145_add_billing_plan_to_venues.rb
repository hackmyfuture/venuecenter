class AddBillingPlanToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :billing_plan_id, :integer
  end
end
