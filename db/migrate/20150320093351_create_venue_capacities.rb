class CreateVenueCapacities < ActiveRecord::Migration
  def change
    create_table :venue_capacities do |t|
      t.string :name

      t.timestamps null: true
    end
  end
end
