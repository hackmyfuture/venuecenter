class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :venue, index: true, foreign_key: true
      t.references :billing_plan, index: true, foreign_key: true
      t.string :stripe_id

      t.timestamps null: false
    end
  end
end
