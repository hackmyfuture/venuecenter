class RenameQuoteStatusToRfpStatus < ActiveRecord::Migration
  def change
    rename_table :quote_statuses, :rfp_statuses
  end
end
