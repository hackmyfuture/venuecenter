class AddRfpCountToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :rfps_count, :integer, default: 0
  end
end
