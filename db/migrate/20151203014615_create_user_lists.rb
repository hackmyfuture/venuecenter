class CreateUserLists < ActiveRecord::Migration
  def change
    create_table :user_lists do |t|
      t.references :user, index: true, foreign_key: true
      t.references :user_event, index: true, foreign_key: true
      t.string :name
      t.boolean :public

      t.timestamps null: false
    end
  end
end
