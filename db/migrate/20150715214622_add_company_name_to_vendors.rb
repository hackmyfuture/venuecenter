class AddCompanyNameToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :company_name, :string
  end
end
