class AddContactInfoToQuoteRequest < ActiveRecord::Migration
  def change
    add_column :quote_requests, :first_name, :string
    add_column :quote_requests, :last_name, :string
    add_column :quote_requests, :email, :string
    add_column :quote_requests, :session_id, :string
    add_column :quote_requests, :ip_address, :string
  end
end
