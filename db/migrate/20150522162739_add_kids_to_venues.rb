class AddKidsToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :pool, :boolean
    add_column :venues, :kid_friendly, :boolean
    add_column :venues, :lgbt_friendly, :boolean
  end
end
