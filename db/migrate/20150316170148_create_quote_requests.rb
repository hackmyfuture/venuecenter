class CreateQuoteRequests < ActiveRecord::Migration
  def change
    create_table :quote_requests, id: false, primary_key: :id do |t|
      t.string     :id
      t.references :user
      t.references :venue
      t.references :event_type
      t.string     :contact_phone
      t.integer    :num_guests
      t.date       :desired_date
      t.boolean    :date_flexible
      t.string     :max_budget
      t.text       :message
      t.boolean    :overnight_needed
      t.string     :status, default: 'New'
      t.timestamps null: false
    end

    add_index :quote_requests, :id, unique: true
  end
end
