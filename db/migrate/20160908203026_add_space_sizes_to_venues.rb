class AddSpaceSizesToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :num_spaces, :integer
    add_column :venues, :largest_space_capacity, :integer
    add_column :venues, :largest_space_size, :integer
    add_column :venues, :guest_rooms, :integer
  end
end
