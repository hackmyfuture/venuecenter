class AddPageviewsToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :pageviews, :integer, default: 0
  end
end
