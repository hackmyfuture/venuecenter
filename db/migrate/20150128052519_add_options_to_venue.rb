class AddOptionsToVenue < ActiveRecord::Migration
  def change
    add_column :venues, :wine, :boolean
    add_column :venues, :beer, :boolean
    add_column :venues, :full_bar, :boolean
    add_column :venues, :outside_alcohol, :boolean
    add_column :venues, :no_alcohol, :boolean
    add_column :venues, :kosher, :boolean
    add_column :venues, :halal, :boolean
    add_column :venues, :vegetarian, :boolean
    add_column :venues, :gluten_free, :boolean
    add_column :venues, :vegan, :boolean
    add_column :venues, :free_parking, :boolean
    add_column :venues, :self_parking, :boolean
    add_column :venues, :valet_parking, :boolean
    add_column :venues, :garage_parking, :boolean
    add_column :venues, :corkage, :boolean
    add_column :venues, :av_hookup, :boolean
    add_column :venues, :handicap_accessible, :boolean
    add_column :venues, :wifi, :boolean
    add_column :venues, :city_view, :boolean
    add_column :venues, :lake_view, :boolean
    add_column :venues, :ocean_view, :boolean
    add_column :venues, :garden_view, :boolean
    add_column :venues, :beachfront, :boolean
    add_column :venues, :stage_available, :boolean
    add_column :venues, :overnight_available, :boolean
  end
end
