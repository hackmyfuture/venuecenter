class AddStripeToVenueTransactions < ActiveRecord::Migration
  def change
    add_column :venue_transactions, :guid, :string
    add_column :venue_transactions, :stripe_id, :string
  end
end
