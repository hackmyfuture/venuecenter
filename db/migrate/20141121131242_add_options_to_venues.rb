class AddOptionsToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :price, :integer
    add_column :venues, :max_capacity, :integer
    add_column :venues, :onsite_catering, :boolean, default: true
    add_column :venues, :offsite_catering, :boolean, default: true
    add_column :venues, :venue_size, :integer
    add_column :venues, :outside_vendors, :boolean, default: true
    add_column :venues, :pet_friendly, :boolean, default: true
  end
end
