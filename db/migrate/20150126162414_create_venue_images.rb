class CreateVenueImages < ActiveRecord::Migration
  def change
    create_table :venue_images do |t|
      t.references :venue
      t.string :public_id
      t.string :version
      t.string :format
      t.string :resource_type
      t.string :url
      t.string :secure_url
      t.boolean :default
      t.timestamps null: false
    end
  end
end
