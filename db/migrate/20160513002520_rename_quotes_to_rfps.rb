class RenameQuotesToRfps < ActiveRecord::Migration
  def change
    rename_table :quote_requests, :rfps
    rename_column :rfps, :quote_status_id, :rfp_status_id
    add_index :rfps, :rfp_status_id
  end
end
