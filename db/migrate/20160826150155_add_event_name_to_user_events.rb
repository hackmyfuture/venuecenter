class AddEventNameToUserEvents < ActiveRecord::Migration
  def change
    add_column :user_events, :event_name, :string
    add_reference :rfps, :user_event, index: true
    add_foreign_key :rfps, :user_events
  end
end
