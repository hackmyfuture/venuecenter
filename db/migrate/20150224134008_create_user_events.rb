class CreateUserEvents < ActiveRecord::Migration
  def change
    create_table :user_events do |t|
      t.references :user
      t.references :event_type
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.integer :num_guests
      t.decimal :budget, precision: 10, scale: 2
      t.text :notes
      t.string :status, default: 'Active'
      t.timestamps null: false
    end
  end
end
