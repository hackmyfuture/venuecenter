class CreateVenueSpaces < ActiveRecord::Migration
  def change
    create_table :venue_spaces do |t|
      t.references :venue, index: true
      t.string :name
      t.integer :square_feet
      t.decimal :width
      t.decimal :length
      t.decimal :ceiling_height
      t.integer :classroom_size
      t.integer :theater_size
      t.integer :banquet_size
      t.integer :reception_size
      t.integer :conference_size
      t.integer :u_shape_size
      t.integer :h_square_size
      t.timestamps null: false
    end
    add_foreign_key :venue_spaces, :venues
  end
end
