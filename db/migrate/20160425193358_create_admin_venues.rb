class CreateAdminVenues < ActiveRecord::Migration
  def change
    create_table :admin_venues do |t|
      t.belongs_to :admin, index: true
      t.belongs_to :venue, index: true

      t.timestamps null: false
    end
  end
end
