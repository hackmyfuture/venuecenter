class AddEventToQuoteRequest < ActiveRecord::Migration
  def change
    add_reference :quote_requests, :user_event, index: true
    add_foreign_key :quote_requests, :user_events
  end
end
