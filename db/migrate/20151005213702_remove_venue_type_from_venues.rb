class RemoveVenueTypeFromVenues < ActiveRecord::Migration
  def change
    remove_column :venues, :venue_type_id, :integer
  end
end
