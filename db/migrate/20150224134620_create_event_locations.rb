class CreateEventLocations < ActiveRecord::Migration
  def change
    create_table :event_locations do |t|
      t.references :user_event
      t.string :location

      t.timestamps null: false
    end
  end
end
