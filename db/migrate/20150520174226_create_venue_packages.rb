class CreateVenuePackages < ActiveRecord::Migration
  def change
    create_table :venue_packages do |t|
      t.references :venue, index: true
      t.string :name
      t.text :description
      t.decimal :price

      t.timestamps null: false
    end
    add_foreign_key :venue_packages, :venues
  end
end
