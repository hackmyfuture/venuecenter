class AddDescriptionToVenueSpaces < ActiveRecord::Migration
  def change
    add_column :venue_spaces, :description, :text
  end
end
