class AddCountryToStates < ActiveRecord::Migration
  def change
    add_column :states, :country_id, :int
    add_column :states, :abbrev, :string

    add_index :states, :country_id
  end
end
