class FixUserEventDate < ActiveRecord::Migration
  def change
    remove_column :user_events, :desired_date
    add_column :user_events, :desired_date, :datetime
  end
end
