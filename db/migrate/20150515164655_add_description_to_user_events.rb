class AddDescriptionToUserEvents < ActiveRecord::Migration
  def change
    add_column :user_events, :description, :text
  end
end
