class AddDescriptionToVenueImages < ActiveRecord::Migration
  def change
    add_column :venue_images, :description, :text
  end
end
