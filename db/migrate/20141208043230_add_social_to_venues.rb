class AddSocialToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :facebook, :string
    add_column :venues, :twitter, :string
    add_column :venues, :google_plus, :string
    add_column :venues, :yelp, :string
  end
end
