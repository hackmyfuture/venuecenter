class AddDetailsToUserEvents < ActiveRecord::Migration
  def change
    add_column :user_events, :company_name, :string
    add_column :user_events, :desired_date, :string
    add_column :user_events, :date_flexible, :boolean
    remove_column :user_events, :budget
    add_column :user_events, :budget, :string
    add_column :user_events, :need_catering, :boolean
    add_column :user_events, :need_alcohol, :boolean
    add_column :user_events, :overnight_accomodations, :boolean

    remove_column :user_events, :start_date
    remove_column :user_events, :end_date
  end
end
