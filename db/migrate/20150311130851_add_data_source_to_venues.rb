class AddDataSourceToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :data_source, :string, default: 'Manual'
  end
end
