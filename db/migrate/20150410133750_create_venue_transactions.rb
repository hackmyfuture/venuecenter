class CreateVenueTransactions < ActiveRecord::Migration
  def change
    create_table :venue_transactions, id: false, primary_key: :id do |t|
      t.string :id
      t.references :venue
      t.string :transaction_type
      t.string :x_response_code
      t.string :x_response_reason_code
      t.string :x_response_reason_text
      t.string :x_auth_code
      t.string :x_avs_code
      t.string :x_trans_id
      t.string :x_description
      t.string :x_amount
      t.string :x_method
      t.string :x_type
      t.string :x_cust_id
      t.string :x_first_name
      t.string :x_last_name
      t.string :x_address
      t.string :x_city
      t.string :x_state
      t.string :x_zip
      t.string :x_country
      t.string :x_phone
      t.string :x_email
      t.string :x_MD5_Hash
      t.string :x_cvv2_resp_code
      t.timestamps null: false
    end
  end
end
