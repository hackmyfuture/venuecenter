class AddVenueToAdmins < ActiveRecord::Migration
  def change
    add_reference :admins, :venue, index: true
    add_foreign_key :admins, :venues
  end
end
