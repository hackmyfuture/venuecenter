class CreateVenueSearches < ActiveRecord::Migration
  def change
    create_table :venue_searches do |t|
      t.references :user
      t.string :ip_address
      t.string :location
      t.text :search_options
      t.integer :num_results
      t.timestamps null: false
    end
  end
end
