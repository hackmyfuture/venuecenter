class AddFlightToQuoteRequest < ActiveRecord::Migration
  def change
    add_column :quote_requests, :need_flight, :boolean, default: false
  end
end
