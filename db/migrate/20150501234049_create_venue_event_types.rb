class CreateVenueEventTypes < ActiveRecord::Migration
  def change
    create_table :venue_event_types do |t|
      t.belongs_to :venue, index: true
      t.belongs_to :event_type, index: true

      t.timestamps null: false
    end
  end
end
