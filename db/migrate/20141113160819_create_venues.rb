class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.references :venue_type
      t.string :name
      t.string :address
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
