class AddLiveVirtualTourCodeToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :live_virtual_tour_code, :text
  end
end
