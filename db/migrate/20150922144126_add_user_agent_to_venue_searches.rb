class AddUserAgentToVenueSearches < ActiveRecord::Migration
  def change
    add_column :venue_searches, :user_agent, :text
  end
end
