class AddCateringToUserEvents < ActiveRecord::Migration
  def change
    remove_column :user_events, :name
    remove_column :user_events, :need_catering
    remove_column :user_events, :need_alcohol

    add_column :user_events, :first_name, :string
    add_column :user_events, :last_name, :string
    add_column :user_events, :catering_type, :string
    add_column :user_events, :alcohol_type, :string
  end
end
