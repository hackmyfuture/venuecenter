class DropUserInfoFromUserEvents < ActiveRecord::Migration
  def change
    remove_column :user_events, :first_name
    remove_column :user_events, :last_name
    remove_column :user_events, :email
  end
end
