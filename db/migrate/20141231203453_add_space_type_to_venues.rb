class AddSpaceTypeToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :space_type, :string
  end
end
