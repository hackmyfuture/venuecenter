class AddZipCodeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :zip_code, :string, limit: 15
    add_column :users, :display_name, :string
  end
end
