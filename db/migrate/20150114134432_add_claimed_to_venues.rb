class AddClaimedToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :claimed, :boolean, default: false
    add_column :venues, :claimed_at, :datetime

    # add_column :managers, :venue_id, :integer

    # add_index :managers, :venue_id
  end
end
