class AddPhoneToUserEvents < ActiveRecord::Migration
  def change
    add_column :user_events, :contact_phone, :string
    add_column :user_events, :ip_address, :string
  end
end
