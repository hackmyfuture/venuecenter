class CreateUserSavedVenues < ActiveRecord::Migration
  def change
    create_table :user_saved_venues do |t|
      t.references :user, index: true
      t.references :venue, index: true
      t.references :user_event, index: true

      t.timestamps null: false
    end
    add_foreign_key :user_saved_venues, :users
    add_foreign_key :user_saved_venues, :venues
    add_foreign_key :user_saved_venues, :user_events
  end
end
