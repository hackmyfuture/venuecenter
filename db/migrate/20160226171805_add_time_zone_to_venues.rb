class AddTimeZoneToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :time_zone, :string, default: 'Central Time (US & Canada)'
  end
end
