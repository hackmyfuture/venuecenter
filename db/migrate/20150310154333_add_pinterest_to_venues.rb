class AddPinterestToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :pinterest, :string
    add_column :venues, :yelp_id, :string
  end
end
