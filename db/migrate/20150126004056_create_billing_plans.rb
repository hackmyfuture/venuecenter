class CreateBillingPlans < ActiveRecord::Migration
  def change
    create_table :billing_plans do |t|
      t.string :name
      t.decimal :price, precision: 7, scale: 2
      t.integer :images
      t.integer :priority
    end
  end
end
