class AddFlightToUserEvent < ActiveRecord::Migration
  def change
    add_column :user_events, :need_flight, :boolean
  end
end
