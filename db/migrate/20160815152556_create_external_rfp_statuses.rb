class CreateExternalRfpStatuses < ActiveRecord::Migration
  def change
    create_table :external_rfp_statuses do |t|
      t.string :name
      t.timestamps null: false
    end
    add_reference :rfps, :external_rfp_status, index: true
  end
end
