class FixAdminVenueId < ActiveRecord::Migration
  def change
    # remove_foreign_key :admins, :venue
    rename_column :admins, :venue_id, :active_venue_id
    add_index :admins, :active_venue_id
  end
end
