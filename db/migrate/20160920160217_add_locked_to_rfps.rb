class AddLockedToRfps < ActiveRecord::Migration
  def change
    add_column :rfps, :locked, :boolean, default: true
  end
end
