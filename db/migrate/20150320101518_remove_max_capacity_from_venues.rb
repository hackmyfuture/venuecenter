class RemoveMaxCapacityFromVenues < ActiveRecord::Migration
  def change
    remove_column :venues, :max_capacity, :integer
  end
end
