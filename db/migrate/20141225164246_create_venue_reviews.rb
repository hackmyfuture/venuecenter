class CreateVenueReviews < ActiveRecord::Migration
  def change
    create_table :venue_reviews do |t|
      t.references :venue
      t.references :user
      t.string     :title
      t.text       :text
      t.integer    :service_quality
      t.integer    :venue_quality
      t.integer    :staff_quality
      t.boolean    :corporate_event
      t.boolean    :party
      t.boolean    :wedding
      t.boolean    :retreat
      t.timestamps
    end
  end
end
