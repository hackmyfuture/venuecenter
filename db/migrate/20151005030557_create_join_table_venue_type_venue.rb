class CreateJoinTableVenueTypeVenue < ActiveRecord::Migration
  def change
    create_join_table :venue_types, :venues do |t|
      # t.index [:venue_type_id, :venue_id]
      # t.index [:venue_id, :venue_type_id]
    end
  end
end
